% !TeX spellcheck = en_US
%%%%%%%%%%%%%%%%%%%%%%%%% INTRODUCTION %%%%%%%%%%%%%%%%%%%%%%%%%
% https://arxiv.org/pdf/1704.06857.pdf
\section{Conclusions}
\label{sec:conclusions}

In this work, we have reviewed a large part of the methods that have been beneficial to reduce the gap between the synthetic and the real-world domains. In that regard, we proposed UnrealROX, a simulator that improves the drawbacks of the different simulators proposed until now. 

First, we analyzed the two main avenues to minimize this gap: photorealism and domain randomization. Then, we discussed the main advantages and disadvantages about some of the most relevant simulators and generators belonging to the Sim-To-Real area\footnote{\url{https://sim2realai.github.io/}}.

As for photorealism, we have concluded that it is one of the most important points for the transfer of synthetic learned models to the real world. For that, we have determined that using a game engine that conforms to the state of the art in terms of rendering techniques leads to great results, as we discussed in Section \ref{sec:str:sim}. We have also commented on the current limitations of real time ray-tracing to improve the overall quality of the final image; at this date, there is not enough computing power on average devices to carry out a perfect ray-tracing setup.

On the other hand, we found that models used in simulators do not reflect the semantic complexity of real-world, and this makes them prone to poor generalizations when it comes to new data. This issue can be improved with domain randomization, which proposes to generate random variations of the synthetic world so that after the model has seen enough variations it can identify the real world as just another variation. However, this type of randomization can lead to meaningless data, as we discussed in Section \ref{sec:str:dr}. Various models, such as \ac{adr} and \ac{sdr} propose an improved paradigm where this randomization gets controlled following different criteria.

As we have commented before, we have reviewed some of the most relevant simulators and generators to the present day. This task has been critical when defining UnrealROX since one of the main objectives of our simulator is to compensate for the shortcomings of other environments. 

UnrealROX is a simulator that implements grasping interaction using an interchangeable skeletal mesh mannequin that reproduces movements recorded previously with a \ac{vr} device. Our simulator is capable of translating seamlessly the operator's movements into the robot's degrees of freedom. In UnrealROX the user can select the type of data to generate, avoiding unnecessary long calculation times. This data will be generated from the multiple points of view placed and configured by the operator, thus making the system more flexible, as well as improving the dataset. All of this accompanied by the base features of \ac{ue4}, which allow the user to import new scenarios and models. In addition, any developer can download and extend UnrealROX, thanks to the fact that the simulator is open sourced and  available at Github\footnote{\url{https://github.com/3dperceptionlab/unrealrox}}. UnrealROX also counts with a polished documentation open to the public. All of this opens up new research avenues for future work, which we will cover in Section \ref{sec:futurework}.

\section{Highlights}
\label{sec:highlights}
The highlights of this work are the following:

\begin{description}
	\item [$\bullet$] In depth study of the Sim-To-Real field analyzing it's two main avenues: photorealism and domain randomization.
	
	\item [$\bullet$] Review and analysis of the most relevant simulators and synthetic data generators to date.
	
	\item [$\bullet$] Proposal and implementation of UnrealROX, a simulator that covers the limitations of other environments.
	
	\item [$\bullet$] In depth documentation for UnrealROX and its components\footnote{\url{https://unrealrox.readthedocs.io}}.
	
	\item [$\bullet$] Various video presentations for RobotriX\footnote{\url{https://www.youtube.com/watch?v=CiRc5tCtCak}}, UnrealROX\footnote{\url{https://www.youtube.com/watch?v=YOiVr2A2TZo}} and UnrealGrasp\footnote{\url{https://www.youtube.com/watch?v=4sPhLbHpywM}}.
	
	\item [$\bullet$] A dataset featured at \textbf{IROS 2018}\footnote{\url{https://www.iros2018.org/}}: The RobotriX \cite{Garcia_Garcia_2018}, a photorealistic indoor dataset for deep learning.
	
	\item [$\bullet$] UnrealGrasp \cite{oprea2019visually}, a realistic grasping system designed for UnrealROX.
	
	\item [$\bullet$] An incoming UnrealHands, a hand pose detection algorithm using synthetic data.
	
	\item [$\bullet$] Generation of synthetic data for segmentation of actions in video sequences. UnrealROX has been used as a tool to generate the dataset from previously labelled sequences of actions.
	
	\item [$\bullet$] Automation of UnrealROX agents and generation of synthetic actions for semantic segmentation.
\end{description}


\section{Future Work}
\label{sec:futurework}
We have been mentioning throughout this work some of the possible future research lines that could be opened to complement this work. Some of them were initially proposed as part of the planning of this project, however, due to time constraints it has been impossible to address some of them. In this section we summarize them to conclude this Thesis:

\begin{description}
	\item [$\bullet$ Interaction mask:] One of the proposed objectives of this project was to create an interaction mask. This method would consist of creating a binary categorization for the objects with which the user interacts in a scene, drawing in white those pixels which correspond to the objects the operator is interacting with, and in black those which don't. Once the interaction mask is created using synthetic data, we can generate an interaction dataset. Finally we can transfer the learned model to the real world to see how it performs.
	
	\item [$\bullet$ Domain Randomization:] In Section \ref{sec:str:urox}, we discussed the future possibility of including domain randomization in UnrealROX. This would consist of having a total control over the assets in the scene, trying get feasible scenarios as \ac{sdr} proposes. A possible approach might be to create different arrays of meshes and assign predefined random points in the scene where they would appear. We could also control the materials these assets spawn with and define random lighting options for the scene.
	
	\item [$\bullet$ Ray-tracing:] We have analyzed in depth the current state of the art of ray-tracing at the beginning of this work, and we concluded that to use ray-tracing in a feasible manner there needs to be a selective way to decide where to trace more or less rays. As of today, \ac{ue4} implements a ray-tracer in its 4.22 version; however, the technology is still too primitive to be used directly in real time without a major performance hit and framerate drops in \ac{vr}.  
	
	\item [$\bullet$ Non-rigid objects manipulation:] This is the most complex research option out of the proposed ones, since the support for non-rigid objects in \ac{ue4} is very limited. One of the few presences of non rigid objects in the engine is the clothing tool\footnote{\url{https://docs.unrealengine.com/en-us/Engine/Physics/Cloth/Overview}}, but the integration is more focused on visuals rather than interaction. Integrating non-rigid interactive objects in game engines is one of the future state of the art challenges when it comes to physics in real time. Deforming a shader is not enough to bring collision data to the CPU, which is the main research topic of this matter.
\end{description}