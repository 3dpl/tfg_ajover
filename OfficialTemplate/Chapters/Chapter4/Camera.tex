% !TeX spellcheck = en_US
%%%%%%%%%%%%%%%%%%%%%%%%% INTRODUCTION %%%%%%%%%%%%%%%%%%%%%%%%%
\section{Multi-camera Support}
\label{sec:mcs}
If we look at any of the robots available on the market, we can see that almost all of them integrate multiple cameras in different parts of their bodies. These cameras can be used for any purpose the developers require. In addition, external cameras are usually added to the system to provide external (non-egocentric) data to he robot. In \textit{UnrealROX}, we want to simulate the ability to add multiple cameras in a synthetic environment with the goal in mind of having the same or more amount of data that we would have in a real environment. For instance, in order to train a data-driven grasping algorithm it would be needed to generate synthetic images from a certain point of view: the wrist of the robot. To simulate this situation in our synthetic scenario, we would like to give the user the ability to place cameras attached to sockets in the robot's body, e.g., the wrist itself. Another plausible application scenario would be a pose estimation algorithm which needs static cameras placed in a room. In a real environment, it would be necessary to place them manually to gather data, which can become a tedious and slow task; however, in \textit{UnrealROX} we offer the possibility to place cameras in a user friendly way using the \textbf{\ac{ue4}} editor.

\begin{figure}[h]
	\centering
	\includegraphics[width=1\textwidth]{Figures/PepperInteractionIROS}
	\caption{User interacting as Pepper in the scene.}
	\label{fig:pepperinter}
\end{figure}

One of the examples to be highlighted in \textit{UnrealROX} that makes use of this practice is the social robot Pepper (Figure \ref{fig:pepperinter}), thanks to the multi-camera support, we can generate a dataset for Pepper in which it is seen how the user grabs an object from multiple points of view (being one of them the robot's wrist). This dataset can be used to teach Pepper to pick up objects.

To make this system of multiple cameras possible, both static and attached to bones or sockets, we will use \textit{CameraActor} as the camera class and the \textit{Pawn} class as the entity to which we will attach them. By default, \ac{ue4} does not allow us to precisely attach components in the editor so it is necessary to define a socket-camera relationship in the \textit{Pawn} class. This is due to the fact that it has direct access to the skeleton which we will be attaching some of the cameras.

In this section, we will describe the implementation of the multi-camera subsystem. Firstly, in Section \ref{subsec:multicamera:camera_actor} we will describe the \textit{CameraActor} class itself. Next, in Section \ref{subsec:multicamera:camera_movement},  we will explain how to make those actors to move by attaching them to sockets. At last, Section \ref{subsec:multicamera:implementation} provides all the implementation details needed to put all the components together.


%%%%%%%%%%%%%%%%%%%%%%%%% THE CAMERA CLASS %%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{The Camera Actor}
\label{subsec:multicamera:camera_actor}

The objective of the \textit{CameraActor} class is to render any scene from a specific point of view. This actor can be placed and rotated at the user's discretion in the viewport, which makes them ideal for recording any type of scene from any desired point of view. The main goal in \textit{UnrealROX} is to use these cameras to collect data from arbitrary points of view. The output of this camera is the one that we will use to generate part of the dataset as seen in Figure \ref{fig:camucvinter}.

\begin{figure}[h]
	\centering
	\includegraphics[width=1\textwidth]{Figures/CameraUCVInteractionDiagram}
	\caption{Main execution flow depending on the cameras.}
	\label{fig:camucvinter}
\end{figure}

The \textit{CameraActor} is represented in \ac{ue4} by a 3D camera (shown in Figure \ref{fig:cameractorviewport}) icon and like any other actor, it can be moved, rotated and scaled in the viewport, however scaling the camera will not have any effect on the output rendering.

\begin{figure}[h]
	\centering
	\includegraphics[width=0.8\textwidth]{Figures/cameractorviewport}
	\caption{A \textit{CameraActor} in the viewport rendering a stair.}
	\label{fig:cameractorviewport}
\end{figure}

\newpage
This class has an extensive collection of parameters that the user can manipulate to adjust the camera rendering in the editor. Among these variables\footnote{\url{https://api.unrealengine.com/INT/API/Runtime/Engine/Engine/FPostProcessSettings/index.html}} we can discriminate between different categories:

\begin{description}
	\item [$\bullet$ Camera Settings:] In this category we have parameters such as the projection mode, the field of view and the aspect ratio.
	\item [$\bullet$ Color grading:] In this category we can modify the post-processing of the camera, correcting the gamma or modifying the white balance among other options.
	\item [$\bullet$ Tonemapper:] Here we can modify the values of Slope, Toe, Shoulder, Black clip and White clip of the camera.
	\item [$\bullet$ Lens:] The Lens section exposes various effects, such as chromatic aberration, grain or vignette intensity. We can also modify the bloom, add masks, modify the auto exposure, ect.
	\item [$\bullet$ Other rendering features:] In this section we will be able to adjust the post processing materials, the environmental occlusion, the blur and the global illumination in addition to other similar parameters.
\end{description}
As can be seen, \ac{ue4} has a very complete camera class, which we will use throughout the project to render our scenes. In addition UnrealROX will expose the camera options most demanded by the user as is the case of the field of view or the aspect ratio, as well as additional features not included by default in the actor, such as the possibility of creating stereo vision \cite{Munro2006StereoVC}, done by placing automatically in generation time an additional camera next to the original one with the same settings.

%%%%%%%%%%%%%%%%%%%%%%%%% THE CAMERA MOVEMENT %%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Camera Movement}
\label{subsec:multicamera:camera_movement}

Since some of the points of view are dynamic, e.g., robot's wrists, we need to make the cameras move with them. To achieve that, an attaching operation where the camera actors will be parented to the component's point of view that we want to record will be needed. In this regards, understanding the attaching operations the engine provides (as can be observed in Listing \ref{code:attachactor}) is essential.

\begin{lstlisting}[language=C++, caption=Attaches an actor to a parent actor., label=code:attachactor, frame=single]
void AttachToActor
( 
	AActor * ParentActor,
	const FAttachmentTransformRules & AttachmentRules,
	FName SocketName 
)
\end{lstlisting}
\vspace{5mm}

The \textit{AttachToActor} function is in charge of parenting one actor with another following some attachment rules. In addition, we can specify on which socket we want to attach the object. This means that when the selected socket changes its transform, the attached object will change it too according to the \textit{AttachmentRules}. These rules specify how this new attached actor will behave when the socket it is linked moves or rotates. 

The \textit{AttachmentRules} can be defined separately for location, rotation, and scale (we can choose between these three options defined on the \textit{EAttachmentRule} enumeration\footnote{\url{https://api.unrealengine.com/INT/API/Runtime/Engine/Engine/EAttachmentRule/index.html}}). Each entry will modify the attached actor (or component) behaviour in the following way:

\begin{description}
	\item [$\bullet$ KeepRelative:] Keeps current relative transform as the relative transform to the new parent.
	\item [$\bullet$ KeepWorld:] Automatically calculates the relative transform such that the attached component maintains the same world transform.
	\item [$\bullet$ SnapToTarget:] Snaps transform to the attach point.
\end{description}

%%%%%%%%%%%%%%%%%%%%%%%%% Implementation %%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%% THE CAMERA MOVEMENT %%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Implementation}
\label{subsec:multicamera:implementation}

This problem lead us to define an implicit relatioship between the \textit{CameraActor} and the socket it is attached to, in order to implement this relationship, the \textit{Pawn} class implements a {\textit{USTRUCT}} (the Unreal Engine struct) that has two parameters: the camera itself and the socket name. These properties are accompanied by the \textit{EditAnywhere} meta\footnote{To fully understand the metas, it is necessary to understand the \textit{UPROPERTIES} and the reflection system of \ac{ue4} which can be studied on the following URLs \url{https://www.unrealengine.com/en-US/blog/unreal-property-system-reflection} \url{https://wiki.unrealengine.com/UPROPERTY}}, which makes possible the edition of the properties not only on the \ac{cdo} but also on the instance of the object.

\begin{lstlisting}[language=C++, caption=Struct used at UnrealROX to resolve the Camera-Socket relationship., label=code:ustructinter, frame=single]
USTRUCT()
struct FBoneCam
{
	UPROPERTY(EditAnywhere)
	ACameraActor* CameraActor;
	
	UPROPERTY(EditAnywhere)
	FName SocketName;
};
\end{lstlisting}

Once the relationship between these two components has been sorted out (see Listing \ref{code:ustructinter}), it only needs to be adapted for a scenario in which we can possibly have more than one camera. For this, we will use an array of structs as seen in Listing \ref{code:arrayfbone}.

\begin{lstlisting}[language=C++, caption=Array of structs of the previously defined type., label=code:arrayfbone, frame=single]
UPROPERTY(EditAnywhere, Category = Tracker)
TArray<FBoneCam> BoneCams;
\end{lstlisting}

The user will be in charge of filling the array specified in Listing \ref{code:arrayfbone}. To make the process easier, we exposed these properties to be editable in the editor (see Figure \ref{fig:viewportstruct}).

\begin{figure}[hptb]
	\begin{subfigure}{.45\textwidth}
		\includegraphics[width=\textwidth]{Figures/ArrayStructBone}
		\caption{Representation of the array.}
		\label{fig:g1}
	\end{subfigure}%
	\hfill
	\begin{subfigure}{.45\textwidth}
		\includegraphics[width=\textwidth]{Figures/ArrayStructBoneRep}
		\caption{Pawn Actor with cameras.}
		\label{fig:g2}
	\end{subfigure}
	\caption{In-engine representation of the array of structs. In (\subref{fig:g1}) we can see the representation of the array struct in the instance of the object, while in (\subref{fig:g2}) we see its visual representation in the engine.}
	\label{fig:viewportstruct}
\end{figure}


In order to do this, we will override the \textit{OnConstruction} function (represented in Listing \ref{code:onconstructfunc}) of our \textit{Pawn} class, which is executed every time we alter the Pawn in the viewport. This handle is managed by the engine automatically and it is used to program custom behaviour for a concrete actor in the editor world (OnConstruction function will not execute if we move the actor while playing).

\begin{lstlisting}[language=C++, caption=Called when an instance of this class is placed (in editor) or spawned., label=code:onconstructfunc, frame=single]
virtual void OnConstruction
(
	const FTransform & Transform
)
\end{lstlisting}
\vspace{2mm}

%%%%%%%%%%%%%%%%%%%%%%%%% OnConstruction algorithm %%%%%%%%%%%%%%%%%%%%%%%%%
The \textit{OnConstruction} algorithm will have to automatically manage all the \textit{BoneCams} entering and exiting the array. For this, we will use an extra variable of the same type (\textit{CachedBoneCamsPT}), which will contain the \textit{BoneCams} of the previous execution of the \textit{OnConstruction} function. The objetive under caching this array is to control the cameras that existed in the array in the previous execution that no longer exist (and vice-versa).

\newpage
\begin{algorithm}[!th]
BoneCams := unique\_cameras(BoneCams)\;

\vspace{3mm}
C := BoneCams.intersect\_cameras(CachedBoneCamsPT)\;
\For{x in CachedBoneCamsPT}{
	\If{x \textbf{NOT} in C}{ 
		x.Camera-$>$DetachFromActor(...)\;
	}
}

\vspace{3mm}
\For{b in BoneCams}{
	b.Camera-$>$AttachToComponent(b.socketName)\;
	c := b.found(CachedBoneCamsPT)\;
	\If{c != nullptr \&\& c.socketName != b.socketName}{
		b.SetActorRelativeTransform(FTransform())\;
	}
}

\vspace{3mm}
CachedBoneCamsPT = BoneCams\;

\vspace{3mm}
\caption{Main algorithm to attach and detach camera actors to the relative socket}
\label{algo:BoneCamsAlgo}
\end{algorithm}

As can be seen in Algorithm \ref{algo:BoneCamsAlgo}, the first step is to make sure that there are no repeated cameras in the array, since conceptually we cannot assign the same camera to two different sockets.
The next step is to detach the cameras that were in the array in the previous execution that are no longer in the array. For that, we need to know which cameras are still on the array using an intersection operation, the cameras that are in \textit{CachedBoneCamsPT} but not in the intersection will need to be detached.
Finally, the last step is to attach the \textit{BoneCams} cameras to the appropriate socket. If the camera already existed in the array and the socket has changed, we will set its relative transform to FTransform()\footnote{FTransform default constructor sets all the values to unitary or zero as it follows: FTransform(FVector(0,0,0), FRotator(0,0,0), FVector(1,1,1)) for Location, Rotation and Scale respectively}, since it is possible that the user has defined an offset. Once the process is completed, we will prepare \textit{CachedBoneCamsPT} for the next execution, as can be seen at the end of Algorithm \ref{algo:BoneCamsAlgo}.

\newpage
\subsection{Recapitulation}
\label{subsec:multicamera:recap}

To recap this episode, we will do the complete process described above, starting with placing a camera, seen in Figure \ref{fig:multicam:recap1}. We will have to ignore the camera placed behind the pawn, since it is the one that comes with the pawn and is not a \textit{CameraActor}, it is a \textit{CameraComponent}.

\begin{figure}[h]
	\centering
	\includegraphics[width=\textwidth]{Figures/recap1}
	\caption{Placing a camera.}
	\label{fig:multicam:recap1}
\end{figure}

Next, we will associate this camera to a bone. In order to do that, we have to select the Pawn and find in the exposed properties of the instance of the Pawn a Property called BoneCams as we can see in Figure \ref{fig:multicam:recap2}. This Property is a \textit{TArray} type, so we will see a \textit{+} symbol on it which needs to be clicked to add a new entry.

\begin{figure}[h]
	\centering
	\includegraphics[width=\textwidth]{Figures/recap2}
	\caption{BoneCams Property.}
	\label{fig:multicam:recap2}
\end{figure}
\newpage 

In the added entry we will assign in \textit{CameraActor} the camera we created previously, and in \textit{SocketName}, the bone or socket to which we want to attach it, as Figure \ref{fig:multicam:recap3} shows, we have chosen to attach this camera to the \textit{$hand\_l$} bone of the skeleton of the selected pawn.

\begin{figure}[h]
	\centering
	\includegraphics[width=\textwidth]{Figures/recap3}
	\caption{Adding a new entry in the array.}
	\label{fig:multicam:recap3}
\end{figure}

Then the OnConstruction algorithm executes doing the convenient operations following the execution flow represented in Figure \ref{fig:multicam:recap4}. 

\begin{figure}[h]
	\centering
	\includegraphics[width=\textwidth]{Figures/recap4}
	\caption{Execution flow of the OnConstruction function.}
	\label{fig:multicam:recap4}
\end{figure}

Once the \textit{OnConstruction} function has been executed we will have our camera attached to the socket that we have assigned to it, in this case \textit{$hand\_l$}.
\newpage

We will see this relationship in the editor as it follows: the camera will set its location and rotation following the socket orientation and position, this can be seen in Figure \ref{fig:multicam:recap5}.

\begin{figure}[h]
	\centering
	\includegraphics[width=\textwidth]{Figures/recap5}
	\caption{Camera actor assigned to a socket.}
	\label{fig:multicam:recap5}
\end{figure}

Usually this first setup does not suffice to have a clear viewpoint from a desired location, that is why we allow the user to add a relative offset (location and rotation) to this position.
In our case we have to adjust the camera to the right position to replicate our desired viewpoint (possible point of view example represented in Figure \ref{fig:multicam:recap6}).

\begin{figure}[h]
	\centering
	\includegraphics[width=\textwidth]{Figures/recap6}
	\caption{Camera actor exaggerated offset from original position.}
	\label{fig:multicam:recap6}
\end{figure}

Once we have completed all this process, we are ready (if required) to record from the new defined point of view. To add other points of view we would simply have to add a new entry in the array mentioned before and follow the steps defined above.
\newpage