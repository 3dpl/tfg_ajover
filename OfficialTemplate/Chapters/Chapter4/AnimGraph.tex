% !TeX spellcheck = en_US
%%%%%%%%%%%%%%%%%%%%%%%%% INTRODUCTION %%%%%%%%%%%%%%%%%%%%%%%%%
\newpage
\section{Animation System}
\label{sec:anim}
It is essential to have an animation system to transfer all the movements captured with the virtual reality peripherals to the skeleton we are controlling. In order to do that we will use Persona, which is the set of features of \ac{ue4} for working with skeletal animations and Skeletal Meshes.

In Section \ref{subsec:anim:persona} we will describe Persona, followed by a study of the order of execution of the animations in Section \ref{subsec:anim:execflow}, next, in Section \ref{subsec:anim:ik}, we will see the diverse methods of inverse kinematics used, and finally in Section \ref{subsec:anim:bulknodes}, we will observe and analyze a special animation node that has been created for \textit{UnrealROX}.

%--- PERSONA DESCRIPTION ------------------%
\subsection{Persona}
\label{subsec:anim:persona}
Persona is the name \ac{ue4} gives to the main animation framework of the engine. This framework is composed by several features that are listed bellow: 

\begin{description}
	\item [$\bullet$ Skeleton Editor:] Here we can examine and modify the Skeleton of a Skeletal Mesh. In this editor is where we will be able to add Sockets to our skeletons, it is showcased in Figure \ref{fig:skeleditor}.
	\item [$\bullet$ Skeletal Mesh Editor:] In this editor we can assign default materials to your Skeletal Mesh.
	\item [$\bullet$ Animation Editor:] Here we will be able to work with Animation Sequences and other animation assets, such as Blend Spaces and Animation Montages.
	\item [$\bullet$ Animation Blueprint Editor:] Is where we can create the rules for when and how the animations are played. Here we'll be able to use complex state machines and different blends to make the skeleton of our pawn move.
\end{description}

\begin{figure}[h]
	\centering
	\includegraphics[width=0.93\textwidth]{Figures/skel_editor}
	\caption{\ac{ue4} Skeleton Editor. Source: UE4 documentation.}
	\label{fig:skeleditor}
\end{figure}

The \textbf{Animation Blueprint Editor} will be the main point of study in the following subsections because it is where we will drive all the logic for our animations to play, that is why it's so important to describe the full environment.

%--- EXEC FLOW EVENT AND ANIM GRAPH ------------------%
\subsection{Execution Flow}
\label{subsec:anim:execflow}
The \textbf{Animation Blueprint Editor} has two principal features, the Event Graph and the Animation Graph. Each graph is in charge of a different task, however they are coupled in a way that one needs from the other to be coherent. 

%--- EVENT GRAPH ------------------%
\subsubsection{Event Graph}
\label{subsec:anim:execflow:egraph}
The \textbf{Event Graph} is in charge of retrieving all the variables and functions belonging to our owning pawn using Blueprints as can be seen in figure \ref{fig:anim_event_graph}. 

\begin{figure}[h]
	\centering
	\includegraphics[width=0.82\textwidth]{Figures/anim_event_graph}
	\caption{We can see how we use Blueprints to retrieve from the \textit{ROXBasePawn} variables like \textit{RecordMode} (showcased in red).}
	\label{fig:anim_event_graph}
\end{figure}

This graph holds all the desired logic we want to give our animations. We can create events, functions, sub-graphs and macros, like in any other \ac{ue4} Graph. 

A peculiarity to comment about this Event Graph is the native event that we use to handle all our ticking logic, the \textit{Event Blueprint Update Animation}. As we can see in Figure \ref{fig:anim_event_graph}, this node returns the Delta Time X, which is the time between animation updates that gets cached on a variable for ease of use. However, this delta time is different than the global Tick (Update frame) time. 

Another relevant node that we can observe on the same Figure is \textit{TryGetPawnOwner}, which is exclusive to the Animation Blueprint Editor Event Graph. This node gets the owning pawn of this animation system, however if the owning pawn is invalid, it will retrieve \textit{nullptr}. We take advantage of this node and use it to retrieve every useful value stored on our pawn, for that we will have to Cast to its type (since it inherits from pawn, it is a valid case). 

\begin{figure}[h]
	\centering
	\includegraphics[width=1\textwidth]{Figures/anim_velocity_logic}
	\caption{Velocity Calculation using Blueprints}
	\label{fig:anim_velocity_logic}
\end{figure}

Since we have access to the owning pawn, we can get its location and calculate a speed (Speed = Space/Time) as we can see in Figure 	\ref{fig:anim_velocity_logic}. We calculate the Space by subtracting the Current Location with the previous location (stored on \textit{LastTickLocation} variable).



%--- EVENT GRAPH ------------------%
\subsubsection{Animation Graph}
\label{subsec:anim:execflow:agraph}
The \textbf{Animation Graph} will handle all the logic referred to the animations, it is totally relevant that we analyze deeply the execution order and how several conditions can affect and differ the output. The Figure \ref{fig:anim_anim_graph} shows UnrealROX's Animation Graph along a reduced version of our main \ac{fsm}.

\begin{figure}[h]
	\centering
	\includegraphics[width=1\textwidth]{Figures/anim_anim_graph}
	\caption{UnrealROX full Animation Graph, on the left side of the image we can see the main \ac{fsm}, each circle corresponds to a single animation state, while the arrows refers to the transitions of these states.}
	\label{fig:anim_anim_graph}
\end{figure}

As we can see in the previous Figure, the logic splits in two main branches that relate to the animation mode. UnrealROX counts with two custom modes, \textit{record mode} and \textit{rebuild mode}.

The \textbf{record mode} reads the retrieved variables on the event graph and applies these transformations to the bones through inverse kinematics. This mode plays the animations while the operator controls the \ac{vr} peripherals described in Section \ref{sec:c3:hware:orc}. 

\begin{figure}[h]
	\centering
	\includegraphics[width=1\textwidth]{Figures/anim_ik_example_1}
	\caption{\textit{HeadTransform} variable applied to the head skeleton bone. It makes the head rotate applying inverse kinematics.}
	\label{fig:anim_ik_example_1}
\end{figure}


An example of this can be seen in Figure \ref{fig:anim_ik_example_1} where we get the head transform, that has been calculated on the Event Graph (as we can see in Figure \ref{fig:head_rot_calculated}), and use it to apply a transform to the head bone and relatives, using the \textit{FABRIK}\footnote{\url{https://docs.unrealengine.com/en-us/Engine/Animation/NodeReference/Fabrik}} and the \textit{Transform (Modify) Bone}\footnote{\url{https://docs.unrealengine.com/en-us/Engine/Animation/NodeReference/SkeletalControls/TransformBone}} nodes.

\begin{figure}[h]
	\centering
	\includegraphics[width=1\textwidth]{Figures/head_rot_calculated}
	\caption{HeadRotation calculation based on the Pawn Camera.}
	\label{fig:head_rot_calculated}
\end{figure}

The \textbf{rebuild mode} simply plays back all the movements we recorded previously. These movements, which were stored frame by frame on a text file as transformations, are retrieved in rebuild mode and applied to each related bone with a custom bulk operation (see Figure \ref{fig:anim_bulk_ex_a}) that will be described in Section \ref{subsec:anim:bulknodes}.

\begin{figure}[h]
	\centering
	\includegraphics[width=1\textwidth]{Figures/anim_bulk_ex_a}
	\caption{Bulk Transform (Modify) Bones applies transforms in bulk to specific bones.}
	\label{fig:anim_bulk_ex_a}
\end{figure}

One detail to comment about the previous Figure is the \textit{ScaleHeadSmallAnim} variable, this variable determines if we should reduce the head size to 10\% of the original value. We do that on first person mode, because some artifacts can be seen if the head is in place, so to avoid these artifacts, we reduce the size of the head notably.

We have two separated modes that we need to control separately, for that, we created the \textit{RecordMode} variable we spoke about previously, this variable is in charge of deciding which execution branch will reach to the final Animation Pose. 
\begin{figure}[h]
	\centering
	\includegraphics[width=.7\textwidth]{Figures/anim_branch_vs_blend}
	\caption{Left to Right: Blend Poses by bool and normal branch.}
	\label{fig:anim_branch_vs_blend}
\end{figure}

In order to do that we use the \textit{Blend Poses by Bool} node. This node behaves like a normal branch node, the only difference is that it outputs a pose instead of a execution signal as we can see in Figure 	\ref{fig:anim_branch_vs_blend}.

Apart from the record mode condition, in UnrealROX we also differentiate the situation in which the user does not have the headset on (\textit{IsHMDEEnabledAnim}), so we can avoid the inverse kinematic and grasping logic that is not needed without \ac{vr}. These branches are located at the end of our animation graph as the Figure \ref{fig:anim_main_branches} shows.

\begin{figure}[h]
	\centering
	\includegraphics[width=1\textwidth]{Figures/anim_main_branches}
	\caption{Main animation graph branches.}
	\label{fig:anim_main_branches}
\end{figure}

Figure \ref{fig:anim_main_fsm} shows the main state machine used in UnrealROX, it can be extended with more states and transitions up to the user will. In this case the previously calculated Speed (Figure 	\ref{fig:anim_velocity_logic}) drives the most part of the transition logic. Each state has a final animation pose that feeds our main animation graph, these states can be considered sub-animation graphs.

\begin{figure}[h]
	\centering
	\includegraphics[width=0.9\textwidth]{Figures/anim_main_fsm}
	\caption{UnrealROX main animation finite state machine.}
	\label{fig:anim_main_fsm}
\end{figure}
 

 %--- INVERSE KINEMATICS ------------------%
 \subsection{Inverse Kinematics}
 \label{subsec:anim:ik}
In order to apply several transformations to the skeleton of our pawn coherently, inverse kinematics techniques have had to be employed. Specifically, we have worked with chains of bones to simulate a more natural movement of the skeleton. The only inputs we have available are the position of the hands and head, all of this thanks to the virtual reality controllers. With only these positions we have to make all the body move accordingly in a cohesive way.
 
 \begin{figure}[h]
 	\centering
 	\includegraphics[width=.81\textwidth]{Figures/anim_irlop_vs_inversekinematics}
 	\caption{The poses that the human operator makes get translated to the mannequin appropriately thanks to the inverse kinematic techniques applied.}
 	\label{fig:anim_irlop_vs_inversekinematics}
 \end{figure}
 
In order to get the effect achieved in Figure \ref{fig:anim_irlop_vs_inversekinematics}, we need to open our Animation Graph and analyze each specific skeleton bone that we are controlling using inverse kinematics.

\subsubsection{The FABRIK node}
\label{subsubsec:anim:ik:head}
The bone system of \ac{ue4} classifies the bones of the skeleton through a hierarchy of inheritance, this means that the bones of the fingers belong to the bone of the hand which at the same time belongs to the bone of the arm and so forth.  We can use this hierarchy to define chains of bones affected by the movement of a single bone. This is done by the FABRIK node, which allows us to define a tip bone and a root bone. FABRIK will automatically handle the translations and rotations of the bones that are included in this defined chain as we can see in Figure \ref{fig:anim_ik_example_head}.

\begin{figure}[h]
	\centering
	\includegraphics[width=1\textwidth]{Figures/anim_ik_example_head}
	\caption{FABRIK node and its detail window in context.}
	\label{fig:anim_ik_example_head}
\end{figure}


The following visual representation in Figure \ref{fig:anim_tiprot} represents these two elements, the tip and the root. As we can see we have a direct relationship between the root bone that acts as a support bone and the tip bone, which is the \textit{moving} bone on our bone chain, every movement made by the tip bone will adjust the other bones of this chain, setting their translations and rotations using the root bone as a base.

 \begin{figure}[h]
	\centering
	\includegraphics[width=0.8\textwidth]{Figures/anim_tiprot}
	\caption[]{Basic inverse kinematics scenario. Source: Luis Martinez IK solver\footnotemark.}
	\label{fig:anim_tiprot}
\end{figure}
\footnotetext{\url{https://vimeo.com/246238063}}

The example visualized at Figure \ref{fig:anim_ik_example_head} is using the head as the tip bone and the spine as the root bone. This means that if we incline our head forward, all the bones between the head and the spine will rotate accordingly, however it is impossible to make a total representation of the reality without more tracking points, so the system only tries to represent in the best possible manner the operator's pose.

  %--- BULK NODE --------------------------%
 \subsection{Bulk Transform (Modify) Bones}
 \label{subsec:anim:bulknodes}
 \textit{Bulk Transform (Modify) Bones} is a node that has been created to assign multiple transforms at multiple bones. To understand this node, we have first to take a look at the original \textit{Transform (Modify) Bone} node.
 
\begin{figure}[h]
	\centering
	\includegraphics[width=.8\textwidth]{Figures/anim_transform_modify_bone}
	\caption{Transform (Modify) Bone node.}
	\label{fig:anim_transform_modify_bone}
\end{figure}

This node (represented in Figure \ref{fig:anim_transform_modify_bone}) takes a pose as an input, on the detail panel of the node we can assign the bone we want to modify (in the case of the image \textit{hand\_l}), and then we can define the rules to apply for the new translation, rotation and scale applied to said bone, which modification gets returned on the output pose.

The main problem of this node is that it only allows us to select one single bone, so at the first stages of development it was very complicated to handle every bone in the \textit{rebuild mode}, since the code was enormous. This led us to come up with a solution in which we could assign all these transformations at once without having to create an endless chain of nodes to handle each bone in the skeleton.

\begin{figure}[h]
	\centering
	\includegraphics[width=1\textwidth]{Figures/anim_bulk_node_detail}
	\caption{Bulk Transform (Modify) Bones in detail.}
	\label{fig:anim_bulk_node_detail}
\end{figure}

In Figure \ref{fig:anim_bulk_node_detail} we can see this node applied on the rebuild mode along its detail panel that doesn't differ too much from the original\textit{ Transform (Modify) Bone} node. The node receives a pose and a struct as parameters and returns a pose with the transforms applied. The struct holds a Hash Map that has as a key the name of each bone accompanied by the transform of that same bone as a value. This statement is valid since there cannot be bones on the \ac{ue4} skeleton that have repeated names. 

The functionality of this node consists of modifying each bone to the transform assigned to it, which is why we have opted for a hash-map to create this relationship. That hash-map will be filled at runtime and will change every single \textit{movement iteration} according to the previously \textit{recorded} txt file. 

The struct is necesary due to a limitation of \ac{ue4} that doesn't allow the user to have a hash map as an input for this kind of node, but it is totally redundant. The functionality of this node is as simple as described in Algorithm \ref{algo:BulkModifyBone}.

\begin{algorithm}[!th]
	\For{pair in REC\_NameTransformMap\_Anim}{
		\If{pair.bone in Pose.Bones}{ 
			BoneId := Pose.GetId(pair.bone)\;
			Pose.Bones[BoneId].transform = pair.transform\;
		}
	}
	\vspace{3mm}
	\caption{Very simplistic version of the bulk modify bones algorithm.}
	\label{algo:BulkModifyBone}
\end{algorithm}

\ac{ue4} animation system disposes of specific handles to extract the bone id given a bone name along another useful features, however that is way too specific for the purpose of this document. Algorithm 	\ref{algo:BulkModifyBone} is a very naive and simple version of the real algorithm applied to modify all the transformations for all the bones.

