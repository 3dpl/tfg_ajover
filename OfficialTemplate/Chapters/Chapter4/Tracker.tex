% !TeX spellcheck = en_US
%%%%%%%%%%%%%%%%%%%%%%%%% INTRODUCTION %%%%%%%%%%%%%%%%%%%%%%%%%
\newpage
\section{The Tracker}
\label{sec:tracker}

The Tracker Actor is one of the most important pieces of \textit{UnrealROX}, as it is the class responsible for recording and reproducing the actions of the scene, including the Pawn. 

First in Section \ref{subsec:tracker:usage} we will explain what is the Tracker and how to use it on \textit{UnrealROX}. Then, in Section \ref{subsec:tracker:record}, we will explain how the scene actions are saved to a text file which we later interpret with the playback mechanism explained in Section \ref{subsec:tracker:playback}. We will also see how the tracker gets initialized in Section \ref{subsec:tracker:init}.

%--- INIT -----------------------%
\subsection{Usage}
\label{subsec:tracker:usage}
As we explained in the introduction, the Tracker is the class in charge of recording and reproducing a scene to obtain the dataset. Without a Tracker Actor in a scene none of this can be achieved.

Now that we know what is the responsibility of this Actor, we can explain how to use it inside UnrealROX.

The Tracker can be found under the default folder \textit{C++ Classes} in the Content Browser\footnote{\url{https://docs.unrealengine.com/en-us/Engine/Content/Browser}} (this directory only appears in the content browser if you are working with a C++ \ac{ue4} project) under the robotrix folder in the public category, we can use the searching function of the content browser to find it immediately as we can see in Figure \ref{fig:TrackerEditor}. 

\begin{figure}[h]
	\centering
	\includegraphics[width=1\textwidth]{Figures/TrackerEditor}
	\caption{\ac{ue4} content browser.}
	\label{fig:TrackerEditor}
\end{figure}

As we explained previously, in order to record a scene, we need to add a Tracker to the scene we want to record. To do that, we simply have to click and drag the Tracker Actor from the content browser to the level viewport. This will add a tracker to the scene. We can verify that the Actor is placed in the scene by finding it in the World Outliner, which is located to the right of the level viewport, we can see the placed ROXTracker in the World Outliner in Figure \ref{fig:TrackerEditor}.

Once this Actor is added to the scene, we can click on it on the World Outliner and see it's exposed properties. Figure 	\ref{fig:RecordTracker} shows the first section of the exposed properties of the Tracker.

\begin{figure}[h]
	\centering
	\includegraphics[width=1\textwidth]{Figures/RecordTracker}
	\caption{Tracker Actor generic settings.}
	\label{fig:RecordTracker}
\end{figure}

\newpage
These are the settings we can see in the previous Figure and their description:

\begin{description}
	\item [$\bullet$ Record Mode:] If this value is true, we will be on recording mode, meaning that if we trigger the recording action, the Tracker will start populating a TXT, that will be translated on a further step into a \ac{json} file. If it's set to false we will be interpreting the data of that \ac{json} and reproducing it.
	\item [$\bullet$ Scene Save Directory:] Absolute path in which we will save the \ac{json} data.
	\item [$\bullet$ Scene Folder:] Folder inside the \textit{Scene Save Directory} where we will save the \ac{json}s for every sequence.
	\item [$\bullet$ Generate Sequence \ac{json}:] It transforms the txt data into a \ac{json}. It can be pressed in the editor.
\end{description}

The next properties we can find on the Tracker are under the \textit{Recording} category, which will remain active only if the \textit{Record Mode} boolean is set to true. We can see these settings in Figure \ref{fig:RecordTrackerA}.

\begin{figure}[h]
	\centering
	\includegraphics[width=1\textwidth]{Figures/RecordTrackerA}
	\caption{Tracker Actor recording settings.}
	\label{fig:RecordTrackerA}
\end{figure}

This section has 3 arrays. The first one called \textit{Pawns}, contains the Pawn references that we will be recording, which we will see in detail in Section \ref{subsec:tracker:record}. The second array stores all the camera references that will be tracked. An finally, the third array defines the stereo distance (if any) for these cameras following the same order as the previous array. \textit{Scene File Name Prefix} is a simple naming convention setting.
 
\newpage
The final exposed properties belong to the Playback category on the Tracker as we can see in Figure \ref{fig:PlayBackSettings}.

\begin{figure}[h]
	\centering
	\includegraphics[width=1\textwidth]{Figures/PlayBackSettings}
	\caption{Tracker Actor playback settings.}
	\label{fig:PlayBackSettings}
\end{figure}

\begin{description}
	\item [$\bullet$ Json File Names:] This array stores the input \ac{json}s we generated in the recording phase that the playback system will process.
	\item [$\bullet$ Start Frames:] This array correlates with the Json File Names array, meaning that it allows us to set a certain start frame per \ac{json}. The first array entry will set the first frame to start for the first \ac{json} on the \textit{Json File Names} array.
	\item [$\bullet$ Playback Only:] It \textit{plays} the \ac{json} without generating any data.
	\item [$\bullet$ Playback Speed Rate:] It's only available when we set the Playback Only option to true and it allows us to set the speed at which the playback will play.
	\item [$\bullet$ Generate RGB:] It generates a RGB image per camera per frame, we can adjust the format to JPG-80, JPG-95 (the number is the percentage of quality set to the JPG compression algorithm) or PNG if this boolean is set to true.
	\item [$\bullet$ Generate Depth:] It generates a Depth image per camera per frame.
	\item [$\bullet$ Generate Object Mask:] It generates an Object Mask image per camera per frame. This image gets generated by setting the viewmode to unlit and changing every single instance material to a unique color per object instance.
	\item [$\bullet$ Generate Normal:] It generates a Normals image per camera per frame. We can obtain the normal information of the objects inside \ac{ue4}.
	\item [$\bullet$ Generate Depth Txt Cm:] Generates a text file where each number correlates to the depth value in centimeters of the k pixel of the depth image.
	\item [$\bullet$ Screenshot Folder/Save directory:] It determines where the screenshots will be saved.
	\item [$\bullet$ Generated Images width/Height:] Resolution of the image.
\end{description}


%--- INIT -----------------------%
\subsection{Initialization}
\label{subsec:tracker:init}
Now that we covered all the properties we can start with the logic. In this first section we will study how the Tracker initializes and prepares other classes for the recording and the playback.

If we look at the Tracker \textit{Begin Play}, we can see that we store the original \textit{EngineShowFlags} variable in the \textit{GameShowFlags} variable defined on the Tracker class. ShowFlags are a set of bits that are stored in the ViewFamily that can alter the way a scene renders in \ac{ue4}. We store the original reference of the engine \textit{ShowFlags} to reset its values after we made a change to the \textit{EngineShowFlags} variable in other part of the code as we can see in Listing \ref{code:EngineShowFlags}.

\begin{lstlisting}[language=C++, caption=GameShowFlags variable usage example., label=code:EngineShowFlags, frame=single]
GameShowFlags = new FShowFlags(Viewport()->EngineShowFlags);
// Altering the ShowFlags variable
AlterEngineShowFlags(Viewport()->EngineShowFlags);
// Setting back default value
Viewport()->EngineShowFlags = *GameShowFlags;
\end{lstlisting}
\vspace{5mm}

Following next, \textit{GScreenshotResolutionX} and \textit{GScreenshotResolutionY} are set, these variables are part of the engine and set the resolution of the images that \textit{TakeHighResScreenShot}\footnote{\url{https://api.unrealengine.com/INT/API/Runtime/Engine/FViewport/TakeHighResScreenShot/index.html}} outputs. In UnrealROX there is a functionality that allows us to take screenshots in the runtime by using this function, so the resolution set by the user in the Tracker Actor will be the one this function will be processing.

Next, Pawns get initialized by looping through the Pawn array that we saw in Figure 	\ref{fig:RecordTrackerA}. That loop calls the function \textit{InitFromTracker} (explained in Section \ref{subsec:pawn:conthandler}) in every pawn of this array. The initialization also caches the ControllerPawn variable, which represents the pawn user-controlled pawn.

The camera actors set in the editor get added to an Actor view target array, which we use to swap between different view targets, as we explained in Section \ref{subsec:pawn:conthandler}. To recapitulate, these view targets are the points of view we set in our scene, to do that we place camera Actors in the editor in the locations from where we desire to have a point of view.

\begin{figure}[h]
	\centering
	\includegraphics[width=1\textwidth]{Figures/TrackerBeginPlayModeList}
	\caption{View mode settings.}
	\label{fig:TrackerBeginPlayModeList}
\end{figure}

The last step we do in the generic initialization of the Tracker is filling the \textit{EROXViewModeList} array. As we can observe in Figure \ref{fig:TrackerBeginPlayModeList}, this array is populated based on the settings the user introduces in the editor, as we saw previously in the introduction. This array will be used in the main Tracker algorithm to determine which view modes should be computed.

Finally if we are in playback mode, we call \textit{RebuildModeBegin()}, which will be in charge of \textit{playing} the \ac{json} file.

\textit{PrepareMaterials} is a function that will prepare the object mask materials. This will be done by spawning flat materials and caching the original materials of every single mesh in the scene, it will also assign a color per mesh component. Masked materials are flat colored materials that rendered in Unlit mode, let us segment \textit{Actors} by colors.

%--- RECORD ---------------------%
\subsection{Recording the scene}
\label{subsec:tracker:record}
When we set the Tracker in recording mode, the system is prepared to record the scene we are on. For that, we have the \textit{StartRecording} handler on the Controller, as we explained in Section \ref{subsec:controller:mainbehav}. When that handler is used, the function \textit{ToggleRecording} on the Tracker is called. This function works like a flip flop, meaning that if the recording mode is activated and the function gets called, it will be deactivated and vice-versa. \textit{ToggleRecording} resets the frame counter to zero, so every time we want to record, it will be properly initialized, it also writes the header in the text file we will be writing on this recording phase.

The header is the initial information the playback system (explained in Section \ref{subsec:tracker:playback}) needs to be aware of. The \textit{WriteHeader} function writes this initial part of the TXT. In order to do it, first, prints the number of the cameras the Tracker has on the \textit{CameraActors} array. Then, iterates through this array printing the name of each camera accompanied by certain settings, such as the stereo distance and the field of view. Next, prints the number of static mesh actors tagged as movable contained in the scene and iterates through them printing their names and bounding box information. The following step is printing the number of Pawns that we will be tracking accompanied by their names and socket number (number of bones in the skeleton used). Lastly, the non movable objects information gets printed following the same format as the movable objects. Figure \ref{fig:TrackerSampleHeader} shows a sample header.

\begin{figure}[h]
	\centering
	\includegraphics[width=1\textwidth]{Figures/TrackerSampleHeader}
	\caption{Sample header from the recording TXT file.}
	\label{fig:TrackerSampleHeader}
\end{figure}

Once the header is written and the recording mode is set to true, the Tracker starts calling every frame \textit{WriteScene}, which is the function in charge of writing every tick the information of the scene. To do that, it prints the word frame along the number of the frame processed accompanied by the the time, the frame counter gets increased every time this function gets called. This variable is convenient since it allows us to know the frame related to a specific setup, like an ordered array structure. 

The execution flow of this function goes as it follows: First, we iterate through the camera Actor array, obtaining their names followed by their transform information to then print them in order. Next we iterate through the pawns getting their name and transform information, in addition, we iterate through the sockets of the skeleton of each pawn to obtain the desired socket information. This socket information is what will make the skeleton of our pawns move, since thanks to that we can know the location and the rotation of every socket in the skeleton. The last thing we do is printing the information of every mesh in the scene following the format explained previously. As we said before, this information is retrieved in every tick, and we can see the format in Figure \ref{fig:TrackerSampleAll}.

\begin{figure}[h]
	\centering
	\includegraphics[width=1\textwidth]{Figures/TrackerSampleAll}
	\caption{TXT recording file.}
	\label{fig:TrackerSampleAll}
\end{figure}

In order to write the information to a text file and not lock the main game thread, we have created an Async task\footnote{\url{https://wiki.unrealengine.com/Using_AsyncTasks}} that we use to write strings on a file as we can see on Figure 	\ref{fig:AsyncWriteFile}. This task uses a built in function of \ac{ue4} for writing a string in a file. 

\begin{figure}[h]
	\centering
	\includegraphics[width=1\textwidth]{Figures/AsyncWriteFile}
	\caption{Async task for writing a string on a file.}
	\label{fig:AsyncWriteFile}
\end{figure}

This task has as arguments the absolute path of the file, including the name, besides the string that we want to add to the file.

We mentioned earlier that the playback mode uses a \ac{json} file, and so far we have commented that the data is written in a TXT file. UnrealROX has a feature, already commented in Section \ref{subsec:tracker:usage}, that allows us to transform this generated TXT file into a \ac{json} file. We do this separately because the data in a \ac{json} is more bulky, that means that you have to write more in every frame, hence the performance of the project could be compromised. That's why we generate first a TXT, which is lighter to create. File which we will transform to \ac{json} from the editor, out of runtime.

Once the \ac{json} file is generated, we are ready to use the playback mode.


%--- PLAYBACK -------------------%
\subsection{Reproducing the scene}
\label{subsec:tracker:playback}
Playing a scene from a \ac{json} file in UnrealROX requires a previous setup, which consists of several phases. As we have already explained, the objective of this stage is to generate a dataset, which is composed of images taken in \ac{ue4} from different points of view in a specific scene. 

The first step to generate this dataset is to place as many \textit{Camera Actors} as we need. These actors, as described in Section \ref{sec:mcs}, will be the points of view that will be taken into account when generating the data, in addition to the default camera of the pawns. %%TODO ERASE NEXT? 
These cameras need to be set up on the Tracker in the recording phase, so we can track their position.%%TODO ERASE NEXT? 

Once the \ac{json} is ready, we can set the Tracker Actor on playback mode, to do this, we set the \textit{bRecordMode} variable of the Tracker to false. If it is set to false, \textit{BeginPlay} will call \textit{RebuildModeBegin}, which is in charge of the playback loop. 

The design of the playback mode of the Tracker allows us to define several \ac{json} files to reproduce in an array. This option enables the generation of data from different recordings in a single playback session. By this statement we are defining our first outer loop.

First, we load the \ac{json} file we want to play onto our \textit{JSONParser} class. This class will be in charge of parsing the \ac{json} data into a more \ac{ue4} friendly format. It's also essential to disable the gravity in all the meshes of the scene, since we don't want any physic simulation to happen while we are setting the transform of the objects recorded previously. Once everything is initialized we can start with the inner loop which takes place on the \textit{RebuildModeMain} function. The following Algorithm \ref{algo:TrackerLoop} represents a simplistic version of the playback loop.

\begin{algorithm}[!th]
 	\For{current\_json\_file in json\_file\_names}{
 		\For{frame in current\_json\_numframes}{
 			\For{actor in Scene}{
 				FROXJsonParser* ActData := frame.Find(actor->GetName())\;
 				\If{actor not null}{
 					actor->SetActorTransform(ActData->Transform)\;
 				}
 			}
 			\If{!playback\_only}{
 				RTs := CreateRenderTargets(Cameras->Transform)\;
 				\For{viewmode in viewmodes}{
 					\For{rt in rts}{
 						rt->TakeScreenshot(viewmode)\;
 					}	
 				}
 			}	
 		}
 	}
 	\vspace{3mm}
 	\caption{Very simplistic version of the playback loop execution flow.}
 	\label{algo:TrackerLoop}
 \end{algorithm}

\newpage
Following next, we iterate through all the frames contained in a single \ac{json} file doing the following: All the objects in the scene retrieve and set their transforms for the current frame from the \ac{json} data we explained above. Once all the objects are placed for a specific frame, we can start taking screenshots for that frame (only if the user desires so). 

To take the screenshots, we use the \textit{Render Target}\footnote{\url{https://docs.unrealengine.com/en-us/Engine/Rendering/RenderTargets}} class, since it allows us to define specific render modes to retrieve precise data from the render target instance. To do this, we will spawn one render target per viewmode in the spot where the camera is located; which means that the cameras are only used as a helper class to spawn the necessary render targets in the appropriate locations. 

This means that we will have four render targets per camera: Lit, Depth, Object Mask and Normal. To make this approach more seamless and performant, we have four arrays containing the cached render targets (or scene capturers), one for each mode, SceneCapture\_Depth, SceneCapture\_Lit and so on. These arrays are encoded in a way that the \textit{n} index of the lit array corresponds to the \textit{n} index of the depth array and the remaining arrays. This means that \textit{SceneCapture\_x[n]}, being x the viewmode, represents one single viewpoint.

If we describe the problem at a lower level, we can elaborate that each viewmode consists on a series of techniques applied over each render target. We will see a more complex explanation of these techniques below.

\begin{description}
	\item [$\bullet$ Lit:] Normal RGB lit picture. Gets generated by setting the \textit{Scene capture source}\footnote{\url{https://api.unrealengine.com/INT/API/Runtime/Engine/Engine/ESceneCaptureSource/index.html}} in SCS\_FinalColorLDR mode, the \textit{Render Target Format}\footnote{\url{https://api.unrealengine.com/INT/API/Runtime/Engine/Engine/UTextureRenderTarget2D/RenderTargetFormat/index.html}} in RTF\_RGBA8 mode, and the Gamma correction to 2.2, so the final result is accurtate with what we see in the screen (without this gamma corrections the pictures would be darker).
	\item [$\bullet$ Depth:] This mode represents what a depth camera would do if it would take a picture of the scene. Each pixel represents the distance between the viewer and the surface where this pixel resides, the darker the pixel is, the nearer this surface is from the viewer. It gets generated by setting the \textit{Scene capture source} in SCS\_SceneDepth mode, the \textit{Render Target Format} in RTF\_RGBA16f mode, and the Gamma correction to 0. There is also an option to write the pixel data on a txt, we do that by iterating through the pixel data and printing the content to a TXT file.
	\item [$\bullet$ Object Mask:] This viewmode consists on painting in different colors every object instance in the scene. We do this to differentiate every instance in the screen by its pixels color in a very precise way. In order to do this, we use the BaseColor RenderTarget in combination with plain-colored materials. This material has a color parameter which permits us to set a unique color to every object instance. By doing that we can have a total control of the color that we set to every actor.
	It gets generated by setting the \textit{Scene capture source} in SCS\_BaseColor mode, the \textit{Render Target Format} in RTF\_RGBA8 mode, and the Gamma needs to be set to 1, which is the only notable difference we can find with the Lit mode setup.
	\item [$\bullet$ Normal:] The normal viewmode simply displays the normals\footnote{\url{	http://wiki.polycount.com/wiki/Normal_map}} of the world. This is done by applying a post process material to the render target. The material can be seen in Figure \ref{fig:NormalMat}. It gets generated by setting the \textit{Scene capture source} in SCS\_FinalColorLDR mode, the \textit{Render Target Format} in RTF\_RGBA8 mode, and the Gamma correction remains unmodified.
\end{description}

\begin{figure}[h]
	\centering
	\includegraphics[width=1\textwidth]{Figures/NormalMat}
	\caption{Normal viewmode material.}
	\label{fig:NormalMat}
\end{figure}

The following Figure \ref{fig:dataset_test_samples} shows some samples extracted directly from the dataset in which the viewmodes are demonstrated. One of the most important points when working on this project is to get correct data, that's why we emphasize on the formats for each viewmode, as they are essential to produce a quality dataset. Continuous studies have been made of the generated data with software such as IrfanView, explained in Section \ref{subsubsec:c3:sware:dce:irfan}, to corroborate the produced results.

\begin{figure}[h]
	\centering
	\begin{subfigure}[b]{0.475\textwidth}
		\centering
		\includegraphics[width=\textwidth]{Figures/IMG_Rgb}
		\caption[Network2]%
		{{\small RGB viewmode}}    
	\end{subfigure}
	\hfill
	\begin{subfigure}[b]{0.475\textwidth}  
		\centering 
		\includegraphics[width=\textwidth]{Figures/IMG_Depth}
		\caption[]%
		{{\small Depth viewmode}}    
	\end{subfigure}
	\vskip\baselineskip
	\begin{subfigure}[b]{0.475\textwidth}   
		\centering 
		\includegraphics[width=\textwidth]{Figures/IMG_Normal}
		\caption[]%
		{{\small Normal viewmode}}    
	\end{subfigure}
	\quad
	\begin{subfigure}[b]{0.475\textwidth}   
		\centering 
		\includegraphics[width=\textwidth]{Figures/IMG_Mask}
		\caption[]%
		{{\small Object Mask viewmode}}    
	\end{subfigure}
	\caption[ UnrealROX viewmodes raw results. ]
	{\small UnrealROX viewmodes raw results.} 
	\label{fig:dataset_test_samples}
\end{figure}


 