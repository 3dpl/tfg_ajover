% !TeX spellcheck = en_US
%%%%%%%%%%%%%%%%%%%%%%%%% INTRODUCTION %%%%%%%%%%%%%%%%%%%%%%%%%
\newpage
\section{The Pawn}
\label{sec:pawnsito}
The Pawn class, as \ac{ue4} defines it, is the base class of all Actors that can be controlled by the players or the AI. A Pawn is the embodiment of a player or AI actor in the world. This not only translates into the Pawn visually defining how the AI actor or player looks, but also how it interacts with the world. By default, there is a one-to-one relationship between Controllers (see Section \ref{sec:controller}) and Pawns; meaning, each Controller controls only one Pawn.

In UnrealROX, the Pawn (ROXBasePawn) handles the Bone-Camera subsystem that we saw in Section \ref{sec:mcs}. This class is also responsible for defining the Skeletal Mesh that the pawn uses, we can also choose the Animation Graph that this mesh will use, the responsibilities of the Animation Graph are defined in Section \ref{sec:anim}. 

In Section \ref{subsec:pawn:controllercdo} we will see and explain in detail the \ac{cdo} of the ROXBasePawn as well as the initialization functions like \textit{BeginPlay}, then in Section  \ref{subsec:pawn:conthandler} we will describe a number of functions of the pawn that serve to return information to the classes that require it, like the Tracker class, which we will describe in Section \ref{sec:tracker}. Finally in Section \ref{subsec:pawn:grasping} we will explain the grasping system.

%--- CONTROLLER INIT ----------------%
\subsection{Initialization}
\label{subsec:pawn:controllercdo}
The initialization of an Actor in \ac{ue4} has various phases. Once it gets instantiated, the very early phase consists on retrieving the values of the \ac{cdo} of the Actor. These values can be defined on the \textit{Details Panel} of a child Blueprint Actor as seen in Figure \ref{fig:DetailsPanelPawn}. However, if we instantiate the parent C++ class of that Blueprint child, the values will be retrieved directly from the constructor of that class. 

\begin{figure}[h]
	\centering
	\includegraphics[width=1\textwidth]{Figures/DetailsPanelPawn}
	\caption{Excerpt of the editor details panel of the ROXMannequinClass (Child of ROXBasePawn).}
	\label{fig:DetailsPanelPawn}
\end{figure}

Variables need to be marked in the C++ class as UPROPERTY with the specifier EditDefaultOnly to be able to access them from  the details panel in the child Blueprint class (Figure 	\ref{fig:DetailsPanelPawn}). These specifiers work along with the \ac{ue4} reflection system and are defined on their documentation about UProperties\footnote{\url{https://wiki.unrealengine.com/UPROPERTY}}. Besides this, if we create a variable directly in Blueprints, \ac{ue4} will add it to the list of properties in the details panel of that Blueprint class and their childs. 

UnrealROX Pawn variable definition is all contained on the parent C++ AROXBasePawn class, which already contains all the properties inherited from its parent APawn. The following figure \ref{fig:ConstructorVariablesPawn} is a section of the constructor where the non-component variables are initialized.


\begin{figure}[h]
	\centering
	\includegraphics[width=0.4\textwidth]{Figures/ConstructorVariablesPawn}
	\caption{Specific variables initialized at the Pawn constructor.}
	\label{fig:ConstructorVariablesPawn}
\end{figure}

Next we proceed to describe the properties seen in the image:

\begin{description}
	\item [$\bullet$ bRecordMode:] Used to know if we are in recording mode in the current execution.
	\item [$\bullet$ SpeedModifier:] Pawn Movement Speed.
	\item [$\bullet$ isHMDEnabled:] Whether or not switching to stereo is enabled; if it is false, then EnableStereo(true) will do nothing. Defines if the game is in \ac{vr} Mode.
	\item [$\bullet$ bScaleHeadSmall:] Defines if the head of the mannequin should be scaled down, as we can see in Figure \ref{fig:anim_bulk_ex_a}.
\end{description}

\begin{figure}[h]
	\centering
	\includegraphics[width=1\textwidth]{Figures/PawnConstructorRest}
	\caption{ROXBasePawn Constructor body.}
	\label{fig:PawnConstructorRest}
\end{figure}

 The rest of the ROXBasePawn class constructor initializes some of the properties defined in the header, especially components as we can observe in Figure \ref{fig:PawnConstructorRest}.
 
\textbf{MeshComponent} is a variable of type \textit{USkeletalMeshComponent} which receives a USkeletalMesh as a variable, which we can assign in one of the children of this Blueprint. We can set this mesh in C++, but it's inconvenient since we have to work with relative routes to find the editor asset, that's why an inherited Blueprint class is used for these purposes as the Figure 	\ref{fig:MeshComponentProp} shows. 

\begin{figure}[h]
	\centering
	\includegraphics[width=1\textwidth]{Figures/MeshComponentProp}
	\caption{Skeletal Mesh and Blueprint graph assets for the USekeletalMeshComponent.}
	\label{fig:MeshComponentProp}
\end{figure}

\textbf{VRTripod} will be used as the root component for all the \ac{vr} subcomponents. Thanks to the hierarchy of unreal components, if we move the root component, all subcomponents associated with it, will move too. This can be seen visually better in the editor as Figure \ref{fig:ComponentsHierarchyPawn} shows.

\begin{figure}[h]
	\centering
	\includegraphics[width=1\textwidth]{Figures/ComponentsHierarchyPawn}
	\caption{VRCamera and VROrigin are attached to the VRTripod which is acting as the root for this component chain.}
	\label{fig:ComponentsHierarchyPawn}
\end{figure}

At the same time, \textbf{PawnCamera}, which is the first person camera of this pawn class, is attached to \textbf{VRCamera}. 
The Motion controllers are children of VROrigin. These motion controllers are the components in charge of communicating the hand controllers with \ac{ue4}, giving it input information as well as transform information.

The last relevant bit in the constructor class is the \textit{SetupHandsCapsuleColliders} function, which will setup all the colliders for the hands of the Pawn. This function creates as many colliders as we need and places them in the fingers of the mannequin we are working with. It also defines the event dispatchers we need for the collision of a specific collider with a world object.This is specific to the grasping method used in the project. In our case, grasping is based on collision with capsules in the hand of the mannequin, as we will elaborate in Section \ref{subsec:pawn:grasping}.

%----------- BEGIN PLAY -----------%
Once we understand how the \ac{cdo} works, we can explain the latest initialization phase utilized on this specific Actor: The \textit{BeginPlay}. 

As we described in Section \ref{subsec:controller:mainbehav}, this event is triggered for all Actors when the game is started, any Actors spawned after the game is started will have this called immediately.

\begin{algorithm}[!th]
	Super::BeginPlay()\;
	\vspace{3mm}
	\If{GEngine->XRSystem.IsValid()}
	{
		\If{GEngine->XRSystem->IsHeadTrackingAllowed()}
		{
			isHMDEnabled = true\;
		}
	}
	\vspace{3mm}
	\If{!isHMDEnabled}
	{
		PawnCamera->SetRelativeLocation(RelativeCameraPosition)\;
	}	
	\vspace{3mm}
	\For{const EHandFinger Finger : EHandFingerAll}{
		R\_FingerGrips.Emplace(Finger, 0.0f)\;
		R\_FingerOverlapping.Emplace(Finger, false)\;
		R\_FingerBlocked.Emplace(Finger, false)\;
		L\_FingerGrips.Emplace(Finger, 0.0f)\;
		L\_FingerOverlapping.Emplace(Finger, false)\;
		L\_FingerBlocked.Emplace(Finger, false)\;
	} 
	\vspace{3mm}
	\For{i = 0; i < MeshComponent->GetNumBones(); ++i}{
		REC\_NameTransformMap.Emplace(MeshComponent->GetBoneName(i), FTransform())\;
	}
	\vspace{3mm} 
	UpdateBoneCamsArray()\;
	SetupHandsCapsuleCollidersAttachment()\;
	\vspace{3mm} 
	CachedPC = Cast<AROXPlayerController>(GetController())\;
	\vspace{3mm}
	\caption{Updating finger block and calling SmoothGrasp if conditions are met.}
	\label{algo:beginplaypawn}
\end{algorithm}

Next, we will describe Algorithm \ref{algo:beginplaypawn} in order. The first thing we do in BeginPlay is to check if we have some kind of virtual reality device connected. If the answer is yes, we will put isHMDEnabled to true. However, if this is not met, we will put the camera in third person mode by just setting the relative location of it. Following next, we initialize the hash maps for our grasping algorithm by just iterating through the Finger enum (which will be described in Section \ref{subsec:pawn:grasping}). The next loop initializes the NameTransformMap which stores the transformations that must be applied to each bone, it is used in Rebuild mode to receive the joint transformations from the Tracker and send them to the AnimGraph. UpdateBoneCamsArray initializes the \textit{BoneCams} array documented in Section \ref{subsec:multicamera:camera_actor}. Next \textit{SetupHandsCapsuleCollidersAttachment} attaches all the finger colliders to the skeletal mesh so they follow the skeleton animations. Finally, we cache the Controller to avoid extra casting.

%--- CONTROLLER INIT ----------------%
\subsection{Main Handlers}
\label{subsec:pawn:conthandler}
The Pawn is constantly accessed from external classes, that's why we have certain public handlers that let the user retrieve information about it or update a specific variable. In this section we won't discuss the handlers used for the Grasping algorithm, which will be documented in Section \ref{subsec:pawn:grasping}.

The first handler we find, which has to do with the initialization section, is \textbf{InitFromTracker}, this function is called from the Tracker object and sets the recording mode in which we are, also caches the tracker to access it from the Pawn. In addition, if we are in playback mode, we deactivate the user input and disable the \ac{hud}.

The second handler is \textbf{MoveCameraRelative}, this function is accessed by the \textit{ROXPlayerController}, and it simply moves the camera in the desired direction by using a vector as an input, which will work as a position offset. This handler is called from the camera movement functions defined in Section \ref{subsec:controller:mainbehav}.

The next function is \textbf{MoveVRCameraControllersRelative}, which is in charge of moving the \textbf{VRTripod} we saw on Figure \ref{fig:ComponentsHierarchyPawn}. This is called on the Controller when  we want to relocate the \ac{vr} subcomponents in a new location, we do this specifically when we move the camera to a new location, so the \ac{vr} handlers are placed appropriately in relation to the camera (which represents our head on \ac{vr}).

\textbf{Tick} is a function included in the AActor class which gets called once per frame. The tick interval can be altered, but for the scope of this project we don't need to change the tick rate. In ROXPlayerController, the Tick will set the proxy PawnCamera (PawnCameraSub) position to where the CameraComponent of the Pawn is located at by just getting its transform. We use this camera Actor proxy because the real Pawn camera is a UCameraComponent, and the PlayBack algorithm works only with ACameraActors, recapitulating Section \ref{subsec:multicamera:recap}.

The fifth handler is \textbf{CameraPitchRotation}, this function is called from the Controller to modify the PawnCamera pitch rotation.

\textbf{ChangeViewTarget} is a function called from the Tracker object that changes the current active point of view (camera) of the controller. This function is used on the Tracker algorithm to swap between the cameras that are placed in the scene or in the pawn skeleton.

The next handler is \textbf{CheckFirstPersonCamera}, which is called from the Tracker and it checks if the input CameraActor is the proxy camera we use to represent the first person camera (PawnCameraSub). This function will scale down the head and return true if the input camera is the one used to represent the first person pawn camera. The Tracker algorithm calls this function every time a new camera is swapped, that's why we reduce the head when the input camera matches, we don't want artifacts on a first person view.

The last function we will be documenting in this section is \textbf{PrintHUD}. PrintHUD is a function that communicates with the HUD interface to show a screen message. This interface has been documented in Section \ref{sec:major_ui}, where we explain in detail the user interface system of UnrealROX.

In addition to these handlers, ROXBasePawn implements inlined getters for several variables: the record mode, the tracker, the speed modifier, the name transform hash map and the mesh component.

\newpage 

%--- GRASPING -----------------------%
\subsection{Grasping}
\label{subsec:pawn:grasping}

As we stated previously, UnrealROX grasping algorithm is based on the collision produced by the capsules attached to the hand of the skeletal mesh of the Pawn with a world object eligible for this interaction. These capsules need to be placed manually by the user according the skeleton hands. Figure 	\ref{fig:capsuleshand} shows the hands colliders attached to the Mannequin Pawn, which is the Pawn example UnrealROX provides.

\begin{figure}[h]
	\centering
	\includegraphics[width=0.6\textwidth]{Figures/CapsulesHand}
	\caption{Capsule colliders placed manually on the hand.}
	\label{fig:capsuleshand}
\end{figure}

Next, we will explain the algorithm in a more precise way, which is implemented in ROXBasePawn. 

As we explained in Section \ref{subsec:pawn:controllercdo}, the constructor of the Pawn will be in charge of creating and initializing each of these capsules, as well as placing them in the correct position for the Pawn. The initialization method is the same for each of these colliders, as we can see on Figure \ref{fig:ColliderInitHand}.

\begin{figure}[h]
	\centering
	\includegraphics[width=0.8\textwidth]{Figures/ColliderInitHand}
	\caption{Initialization of the Tumb\_3R capsule collider.}
	\label{fig:ColliderInitHand}
\end{figure}

The first line creates the component then we set its parent using the \textbf{SetupAttachment} function. \textbf{SetRelativeTransform} is the function we use to properly place these colliders, and with \textbf{SetCapsuleSize} we can adjust the size of this collider. Next, we activate the Overlap events for this specific collider setting \textit{bGenerateOverlapEvents} to true, and we set up some collision rules. Finally, we can bind the overlapping events to whatever function we have in our class; as soon as it meets the function requirements\footnote{\url{https://api.unrealengine.com/INT/API/Runtime/Engine/Components/UPrimitiveComponent/OnComponentBeginOverlap/index.html}} (same signature as the Overlap engine functions).

As we documented previously, the \textbf{BeginPlay} will iterate through the \textit{EHandFinger} to initialize the grasping maps for both hands. These maps control the finger grips, the overlapping and the block based on certain conditions that we will study later in this document. The initialization is simple, as we can see in Algorithm \ref{algo:beginplaypawn}.

Once everything is initialized, the next step is analyzing the entry points, which are GraspRightHand and GraspLeftHand, as we documented in Section \ref{subsec:controller:mainbehav}. We'll only cover GraspRightHand, since the behavior for both Grasping functions is the same.

\textit{GraspRightHand} works like an axis, it takes a value and it calls a function which will do the grasping algorithm. Since the axis are ticking, we can have a continuous control of the user input, hence the \textit{Grasp} function will be called every tick, the only difference will be the \textit{R\_CurrentGrip} value, which is set to the axis value (how much the user presses a button, like a joystick).

The Grasp function is reused for the both hands thanks to the input parameters. The following list includes all the input parameters as well as an explanation of each of them.

\begin{description}
	\item [$\bullet$ float CurrentGrip:] Current axis value.
	\item [$\bullet$ TMap<EHandFinger, float> \&FingerGrips:] Map that stores the fingers bending value.
	\item [$\bullet$ TMap<EHandFinger, bool> \&FingerOverlapping:] Map that holds the overlapping state of each finger.
	\item [$\bullet$ TMap<EHandFinger, bool> \&FingerBlocked:] Map that determines if a finger should be blocked from bending or not.
	\item [$\bullet$ FName BoneName:] Bone where to attach the grasped object, in this case \textit{hand\_r} or \textit{hand\_l}.
	\item [$\bullet$ AActor* \&OverlappedActor:] Current Overlapping Actor.
	\item [$\bullet$ bool \&isActorAttached:] Defines if the Overlapped Actor is attached.
	\item [$\bullet$ bool \&DisableGrab:] Determines if the current hand can grab an object.
	\item [$\bullet$ AActor* \&AttachedActor:] Holds a reference to the currently grasped Actor.
	\item [$\bullet$ bool \&isActorAttachedOtherHand:] This property will be true if an Actor is attached to the "other hand" and not to the one we are checking in this execution. This is done because we can have two different actors attached at the same time, one on each hand. The overlapped actor we receive can be different for each hand, so we only want isActorAttachedOtherHand and AttachedActorOtherHand in order to check if we are passing the same object from one hand to the other, as this case requires some special logic.
	\item [$\bullet$ bool \&DisableGrabOtherHand:] Determines if the opposite to the current hand can grab an object. It is useful to avoid attaching and detaching the object repeatedly (and quickly) between hands. After detaching from one hand, that hand won't be able to grab any other object until it is completely opened.
	\item [$\bullet$ AActor* \&AttachedActorOtherHand:] Holds a reference to the currently grasped Actor on the other hand.
\end{description}

As we saw, the Grasp function needs information about the currently overlapped actor, this information will be retrieved from the callbacks we defined in the colliders initialization. The Figure \ref{fig:OverlapsGrasp} shows these overlapping functions that will grant this information to the Grasping method.

 \begin{figure}[h]
 	\centering
 	\includegraphics[width=1\textwidth]{Figures/OverlapsGrasp}
 	\caption{Begin and End overlap callbacks for the hand colliders.}
 	\label{fig:OverlapsGrasp}
 \end{figure}

As we can see, the \textbf{SetOverlap} function sets the OverlappingActor (passed by reference) that we will use later on the Grasping method. \textbf{SetOverlapEnd} only updates the overlapping state of that finger by accessing the map.

Now that we know where the data comes from, we can disassemble the Grasp function better. But first, let's define it's purpose.

The Grasp function needs to move all the fingers of the hand following a grasping animation until they collide with something. If a finger of the hand collided with something, it will be blocked on the position it collided, meaning that it won't be able to go through the object. The same process is repeated for the rest of the fingers.

\begin{algorithm}[!th]
	\For{auto Entry : FingerBlocked.CreateIterator()}{
		\vspace{3mm}
		const EHandFinger\& Finger = Entry.Key()\;
		const bool\& bFingerBlocked = Entry.Value()\;
		float fFingerGrip = *FingerGrips.Find(Finger)\;
		\vspace{3mm}
		bool bFingerOverlapping = *FingerOverlapping.Find(Finger)\;
		bool bSmoothGrasp = false\;
		\vspace{3mm}
		...
		\vspace{6mm}
	} 
	\vspace{3mm}
	\caption{Iterating through the fingers array.}
	\label{algo:GraspLoopA1}
\end{algorithm}

First, we iterate through the fingers array caching out all the information we need for its post process as we can see in Algorithm \ref{algo:GraspLoopA1}.

Following next, if the finger in this map element is blocked, then we update the block state of this finger to false and set \textit{bSmoothGrasp} to true to call it afterwards.

\begin{algorithm}[!th]
	\vspace{3mm}
	...\\
	\vspace{3mm}
	\eIf{bFingerBlocked}
	{
		\vspace{3mm}
		\If{CurrentGrip < fFingerGrip}
		{
			FingerBlocked.Emplace(Finger, false)\;
			bSmoothGrasp = true\;
		}
		\vspace{3mm}
	}{
		\vspace{3mm}
		bSmoothGrasp = true\;
		\vspace{3mm}
	}
	\vspace{3mm}
	\If{bFingerBlocked}
	{
		\vspace{3mm}
		SmoothGrasp(FingerGrips, Finger, CurrentGrip, 0.6f)\;
		\vspace{3mm}
	}	
	\vspace{3mm}
	\caption{Updating finger block and calling SmoothGrasp if conditions are met. }
	\label{algo:GraspLoopA2}
\end{algorithm}

\textit{SmoothGrasp} simply updates smoothly the position of the fingers by accessing the \textit{FingerGrips} hash map having in consideration the delta time to achieve a slow and smooth grasp movement. Then, we retrieve overlapping information to know if an object can be grasped, as we can see in Figure \ref{fig:IsGrasping}.

 \begin{figure}[h]
	\centering
	\includegraphics[width=1\textwidth]{Figures/IsGrasping}
	\caption{The three important fingers to consider an object to be grasped are the thumb, the index and the middle finger.}
	\label{fig:IsGrasping}
\end{figure}

Then based on that information, we will decide if we should grasp or not an object. Specifically the rules would go as it follows: If the thumb is overlapping along the index or the middle finger, then we can attach the overlapped actor to the proper hand socket, however in order to do this, the actor can't be already attached and the hand can't be marked as \textit{Disabled}. If we grasp an object that is already hold by the other hand, we will just detach this object from the other hand and attach it to the hand we are grasping with. If the first condition wasn't met and we have an actor attached, we'll just detach it.

To attach and detach actors to the hands of the skeleton, we'll use the built in \ac{ue4} functions \textit{AttachToComponent}\footnote{\url{https://api.unrealengine.com/INT/API/Runtime/Engine/Components/USceneComponent/AttachToComponent/index.html}} and \textit{DetachFromComponent}\footnote{\url{https://api.unrealengine.com/INT/API/Runtime/Engine/Components/USceneComponent/DetachFromComponent/index.html}}. The latter needs to re-enable the physics of the object as well as enable its collision.




