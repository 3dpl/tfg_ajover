% !TeX spellcheck = en_US
%%%%%%%%%%%%%%%%%%%%%%%%% INTRODUCTION %%%%%%%%%%%%%%%%%%%%%%%%%
\newpage
\section{The Player Controller}
\label{sec:controller}
\ac{ue4} describes the \textit{PlayerController} as the interface between the Pawn and the human player controlling it. The \textit{APlayerController} class inherits from the \textit{AController} class which is a non-physical Actor that can possess a Pawn (or Pawn-derived class) to control its actions.

Controllers receive notifications for many of the events occurring for the Pawn they are controlling. This gives the Controller the opportunity to implement the behavior in response to this event, intercepting the event and superseding the Pawn's default behavior.

In UnrealROX, the Player Controller (ROXPlayerController) is used as the main input handler class. This is where all the input propagates to the specific classes that need an entry point behavior. Another use we give the Controller is handling the movement of the controlled pawn.

In Section \ref{subsec:controller:inputbind} we list all the inputs, then in Section \ref{subsec:controller:mainbehav} we will briefly describe the functionality of each one of those inputs as well as other details that will help for further implementations. 

%--- INPUT LISTING ------------------%
\subsection{Input Definitions}
\label{subsec:controller:inputbind}
In order to add input functionality to a \textit{PlayerController} in \ac{ue4}, we need to override the \textbf{SetupInputComponent()} function defined on the base \textit{PlayerController} class. This function adds a \textit{UInputComponent} to the Controller, and this component allows us to define inputs by name and add behavior to them as shown in the figure \ref{fig:InputBindingsList}.

\begin{figure}[h]
	\centering
	\includegraphics[width=1\textwidth]{Figures/InputBindingsList}
	\caption{UnrealROX input definition.}
	\label{fig:InputBindingsList}
\end{figure}

The Input component exposes two functions that helps the user to define a specific behavior for a specific axis or action. 

\begin{description}
	\item [$\bullet$ Bind Axis:] Binds a delegate function an Axis defined in the project settings.
	\item [$\bullet$ Bind Action:] Binds a delegate function to an Action defined in the project settings.
\end{description}

\newpage
Action Mappings are for key presses and releases, while Axis Mappings allow for inputs that have a continuous range. These mappings can be configured on \ac{ue4} under \textit{"ProjectSettings/Input"} as can be seen in Figure \ref{fig:ProjectSettingsInput}.

\begin{figure}[h]
	\centering
	\includegraphics[width=1\textwidth]{Figures/ProjectSettingsInput}
	\caption{Inputs defined at Project Settings.}
	\label{fig:ProjectSettingsInput}
\end{figure}

This is where we define the default keys-action/axis relationships of our project, as we can see these names need to match with the action/axis names we set in the controller on the \textbf{SetupInputComponent()} function as we saw in Figure 	\ref{fig:InputBindingsList}.


%--- CONTROL ------------------%
\subsection{Main Behavior}
\label{subsec:controller:mainbehav}
Before looking at all input functions seen above, it is necessary to comment on the BeginPlay function of \textit{ROXPlayerController}. BeginPlay is a function that gets called when an Actor object got properly instantiated and initialized. Since APlayerController belongs to the AActor inheritation chain and it's an available function to use, we can override it to implement any behavior when the Actor is created. On algorithm \ref{algo:beginplaycontroller} we can see a simplistic version of the \textit{ROXPlayerController} BeginPlay.

\begin{algorithm}[!th]
	CachedPawn = Cast<AROXBasePawn>(this->GetPawn())\;
	\vspace{3mm}
	\If{XRSystem->IsHeadTrackingAllowed()}
	{
		isHMDEnabled = true\;
		HMDDvcType = XRSystem->GetHMDDevice()->GetHMDDeviceType()\;
	}
	\vspace{3mm}
	\caption{On the first line we cache the casted Pawn in order to avoid casting every time we need to access any specific feature of our subclass (GetPawn() returns APawn type). Next, we gather information about the VR device in use. }
	\label{algo:beginplaycontroller}
\end{algorithm}

Once we have all this information cached, we can look at the rest of the code that is handled by the input. In order to follow the subsequent points it is recommended to look at Figure \ref{fig:InputBindingsList}.

\begin{description}
	\item [$\bullet$ Move functions:] Move forward and move right simply move the pawn onto the desired direction applying an acceleration formula, this acceleration formula is defined at \textbf{JoystickAxisTunning(float x)}.
	\item [$\bullet$ Turn and Look:] These four functions are in charge of rotating the camera according to the user view.
	\item [$\bullet$ MoveCameraUpDown:] This function moves on the Z axis the camera location at the user will.
	\item [$\bullet$ Grasp functions:] Propagate input to the pawn, which will handle the grasping of each hand.
	\item [$\bullet$ Move Modifiers:] These functions toggle the functionality of moving the camera with the axis.
	\item [$\bullet$ Start Recording:] It toggles the recording function of the tracker by accessing the tracker from the cached pawn.
	\item [$\bullet$ Reset VR:] Executes the console command "vr.HeadTracking.Reset" which resets the VR virtual headset position to the origin.
	\item [$\bullet$ ShowCamTexture:] It is the input handler to activate the cam texture, as seen in Figure \ref{fig:scenecapture}.
	\item [$\bullet$ Restart Level:] It restarts the current level, meaning that all the objects we manipulated in runtime will return to their original state.
	\item [$\bullet$ Start/Stop Profiling:] Delegates input to the HUD to handle this profiling function as described in Section \ref{sec:major_ui}.
	\item [$\bullet$ ShowDebugging:] Toggles on and off the \ac{ue4} stats handled in the HUD class.
	\item [$\bullet$ Change Lit/Vertex/Depth/Normal:] Propagates input to the tracker through the cached pawn in order to change the view mode.
	\item [$\bullet$ Camera Next/Prev:] Manual handlers for the camera iterator to set a camera for the controlled pawn, this is all controlled by the tracker so we need to access it through the cached pawn.
	\item [$\bullet$ Take Screenshot:] Calls this function on the tracker through the cached pawn.
	\item [$\bullet$ Set Record Settings:] Prepares the map for the recording state. This is partially handled by the controller and the tracker. The controller sets the viewmode to unlit and the \ac{fps} to 60 to record at that rate, while the tracker will erase every single light actor of the scene to get the most performance friendly recording environment.
\end{description}
