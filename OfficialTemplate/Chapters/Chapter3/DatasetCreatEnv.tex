% !TeX spellcheck = en_US
%%%%%%%%%%%%%%%%%%%%%%%%% UNREAL ENGINE 4 %%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Unreal Engine 4}
\label{subsubsec:c3:sware:dce:ue4}
If we want to define \ac{ue4}, we can look directly at the definition that Epic Games give in their official web page\footnote{\url{https://www.unrealengine.com/en-US/features}}: 
\textit{"Unreal Engine 4 is a complete suite of development tools made for anyone working with real-time technology"}. \ac{ue4} is the tool we use to generate the first layer of data, in order to get to that point we had to build a project named \textit{UnrealROX} that counts with a set of tools which allows the user to record their actions in a realistic scene, and then be able to reproduce them frame by frame exporting the desired images (depth, rgb, normals and/or mask).  

\ac{ue4} also counts with a custom scripting language called \textbf{Blueprints} that allows us to define custom behaviors in a very easy way, we will see some of its features later in Section \ref{subsec:anim:execflow}. Blueprints have played a substantial role when developing UnrealROX, since they allow quick iterations to create a prototype, which can be later converted and extended in C++.

Some of the main advantages of Unreal Engine that have been beneficial for the complete development of UnrealROX are the following: 

\begin{description}
	\item [$\bullet$ Active development:] \ac{ue4} is on continuous development due to how demanding the gaming industry is. This makes the engine perfect for current state of the art studies in modern rendering techniques. One recent example is the inclusion of ray-tracing technology in real time, as we commented in Section \ref{sec:str:ph}. Also, the staff tries to fix as soon as possible all the bugs that might appear between versions.
	\item [$\bullet$ Big community:] \ac{ue4} disposes of a very active community located in the official forums\footnote{\url{https://forums.unrealengine.com/}}, the unofficial \textit{UnrealSlackers} Discord\footnote{\url{https://unrealslackers.org/}
	} and the \textit{\ac{ue4} AnswerHub}\footnote{\url{https://answers.unrealengine.com}}, where continuous questions are answered of several problems from various topics. \ac{ue4} marketplace is where creators sell or give out \ac{ue4} assets for free , which is the case of some of the scenes we employed in the creation of \textit{The RobotriX} dataset \cite{Garcia_Garcia_2018}.
	\item [$\bullet$ Blueprints:] This is one of the most powerful tools that the engine disposes. It started as \textit{Unreal Kismet}\footnote{\url{https://www.youtube.com/watch?v=IReehyN6iCc}} and has been evolving since the first version of Unreal Engine 4. The set of tools available in Blueprints makes possible to prototype quickly any project using only this visual scripting language. The only inconvenience is that it is binary based, which hampers version controlling.
\end{description}

Figure \ref{fig:answerh} displays how Blueprints look and the \ac{ue4} AnswerHub community.

\begin{figure}[hptb]
	\begin{subfigure}{.49\textwidth}
		\includegraphics[width=\textwidth]{Figures/blueprints}
		\caption{Blueprints.}
		\label{fig:howto}
	\end{subfigure}%
	\hfill
	\begin{subfigure}{.49\textwidth}
		\includegraphics[width=\textwidth]{Figures/answerhub}
		\caption{UE4 AnswerHub.}
		\label{fig:answerhub}
	\end{subfigure}
	\caption{This figure showcases some of the advantages of UE4. In Figure \ref{fig:answerhub} we can see some members of its community.}
	\label{fig:answerh}
\end{figure}

\newpage
In Figure \ref{fig:daynightroom} we can see the capacity of \ac{ue4} to render photorealistic scenes.

\begin{figure}[h]
	\centering
	\includegraphics[width=0.8\textwidth]{Figures/daynightroom}
	\caption{Snapshots of the daylight and night room setup for the Realistic Rendering released by Epic Games to showcase the realistic rendering capabilities of UE4.}
	\label{fig:daynightroom}
\end{figure}

However, it's not all about advantages, since \ac{ue4} has some inconveniences and inconsistencies we had to deal with:

\begin{description}
	\item [$\bullet$ Regressive bugs:] \ac{ue4} has some inconsistencies when it comes to Blueprint-C++ communication. From cyclical dependencies that produce \ac{cdo} resets, to blueprintable C++ structs that get corrupted after inserting members on the type. As I mentioned before, these bugs get addressed when the staff is able to reproduce them, but they eventually come back between versions, which makes updating projects of \ac{ue4} harder.
	\item [$\bullet$ C++ compile times:] This is not exactly a \ac{ue4} problem, but since \ac{ue4} uses C++ we can include it to the list. When not using a very powerful setup, compiling a project can take minutes, which slows down considerably the development process. Current versions of \ac{ue4} have improved the situation with Live++\footnote{\url{https://molecular-matters.com/products_livepp.html}}.
\end{description}

Fortunately, the advantages far outweigh the disadvantages for the scope of this project, which is why we have decided to go ahead with Unreal Engine despite the inconveniences discussed.

%%%%%%%%%%%%%%%%%%%%%%%%% VISUAL STUDIO %%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Visual Studio}
\label{subsubsec:c3:sware:dce:vs}
Microsoft Visual Studio is an \ac{ide} from Microsoft. It is used to develop computer programs as well as websites, web applications, web services and mobile applications. 

Like any other IDE, it includes a code editor that supports syntax highlighting and code completion using IntelliSense (see Figure \ref{fig:visualstudio}). It also includes a debugger that works both as a source-level debugger and as a machine-level debugger. In addition, Visual Studio counts with a great quantity of plugins to ease development.

\ac{ue4} is designed to integrate seamlessly with Visual Studio\footnote{\url{https://docs.unrealengine.com/en-us/Programming/Development/VisualStudioSetup}}, allowing the user to quickly and easily make code changes in its projects to immediately see results upon compilation. Configuring Visual Studio to work with Unreal Engine can help improve the efficiency and overall user experience of developers using Unreal Engine. Also, Visual Studio has support for extending the debugger with visualizers that allow easy inspection of common Unreal types such as FNames and dynamic arrays.In \textit{UnrealROX} we've complemented the usage of Visual Studio with Visual Assist, a tool we will describe bellow.

\begin{figure}[h]
	\centering
	\includegraphics[width=1\textwidth]{Figures/visualstudio}
	\caption{Microsoft Visual Studio environment.}
	\label{fig:visualstudio}
\end{figure}

%%%%%%%%%%%%%%%%%%%%%%%%% VISUAL STUDIO %%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Visual Assist}
\label{subsubsec:c3:sware:dce:vax}
Visual Assist is a plugin for Microsoft Visual Studio developed by Whole Tomato Software. The plugin mainly improves IntelliSense\footnote{\url{https://docs.microsoft.com/en-us/visualstudio/ide/using-intellisense?view=vs-2015}} and syntax highlighting. It also enhances code suggestions, provides refactoring commands, and includes spell-check support for comments. It can also detect basic syntax errors such as the use of undeclared variables. Visual Assist includes features specific to development with \ac{ue4}, including support for UE4 keywords, preprocessor macros, and solution setup. It also includes a series of bindings that allow us to navigate more easily through the project and engine code. These are some of the features we have used:

\begin{description}
	\item [$\bullet$ Find Symbol in Solution:] It allows the user to list all the symbols of the project filtered by a string. It can be fast acceded pressing Shift+Alt+S. 
	\item [$\bullet$ GoTo Implementation:] Jumps to the declaration or implementation of the current symbol. Its shortcut is Alt+G.
	\item [$\bullet$ Open File in Solution:] Opens a dialog of filenames filtered by a string with the shortcut Shift+Alt+O.
\end{description}


%%%%%%%%%%%%%%%%%%%%%%%%% PYTHON %%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Python}
\label{subsubsec:c3:sware:dce:python}
Python is an interpreted programming language whose philosophy emphasizes a syntax that favors readable code.

It is a multiparadigm programming language, as it supports object-oriented, imperative programming and, to a lesser extent, functional programming. It is an interpreted language, uses dynamic typing and is multiplatform.

It is administered by the Python Software Foundation. It has an open source license, called Python Software Foundation License\footnote{\url{https://docs.python.org/3/license.html}}, which is compatible with the GNU General Public License from version 2.1.1, and incompatible in certain earlier versions.

Python has been used to extend the data generation pipeline with an additional ground truth generator module to produce extra data from the raw images and information. One of the multiple tasks in which Python has been used is to generate visible bounding boxes of the objects in a scene (as seen in Figure \ref{fig:python_bound_boxes}). %It has also been used as the main programming language to define our neural network architectures using PyTorch, which will be described on subsection \ref{subsec:c3:sware:ai}.

\begin{figure}[h]
	\centering
	\includegraphics[width=1\textwidth]{Figures/python_bound_boxes}
	\caption{Bounding box generation using Python. These images are part of the UnrealROX dataset.}
	\label{fig:python_bound_boxes}
\end{figure}

%%%%%%%%%%%%%%%%%%%%%%%%% irfanView %%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{irfanView}
\label{subsubsec:c3:sware:dce:irfan}
IrfanView is an image viewer, editor, organiser and converter program for Microsoft Windows. IrfanView is specifically optimized for fast image display and loading times. It supports viewing and saving of numerous file types including image formats such as \textit{BMP}, \textit{GIF}, \textit{JPEG}, \textit{JP2} and \textit{PNG} between others.

IrfanView allows us to verify that the images are generated properly by observing the \ac{rgb} value of every pixel using the color picker tool from the paint dialog, which can be enabled by pressing F12, as we can see in Figure \ref{fig:irfanview}.

\begin{figure}[h]
	\centering
	\includegraphics[width=1\textwidth]{Figures/irfanview}
	\caption{irfanView and infanView paint dialog.}
	\label{fig:irfanview}
\end{figure}


%%%%%%%%%%%%%%%%%%%%%%%%% Sublime %%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Sublime Text Editor}
\label{subsubsec:c3:sware:dce:subl3}
Sublime Text is a sophisticated text editor for code, markup and prose. The following description represents the main features of Sublime Text editor:

\begin{description}
	\item [$\bullet$ GoTo anything:] Quick navigation to files, symbols or lines. With extremely long JSON files, this feature has been one of the most important ones when it comes to verify the data. This enabled us quick navigation between our files.
	\item [$\bullet$ The Command Palette:] Uses adaptive correspondence for quick invocation of arbitrary commands from the keyboard.
	\item [$\bullet$ Simultaneous editing:]  Simultaneously make the same interactive changes in several selected areas. This feature allowed us to fix different JSON fields after main project refactors, meaning that we could fix previously recorded datasets to work with the up to date version.
	\item [$\bullet$ Extensive customization capability:] Through JSON configuration files, including project-specific and platform-specific configuration.
	\item [$\bullet$ Cross-platform:] Cross-platform (Windows, macOS and Linux) and cross-platform support plugins.
\end{description}

Thanks to that set of features, Sublime Text has been the main text editor used when developing the Python Scripts for the dataset. It also has been a great tool for text logging verification due to the extensive lookup functions that Sublime Text disposes, such as the regular expresion tool.

