% !TeX spellcheck = en_US
%%%%%%%%%%%%%%%%%%%%%%%%% PURPOSE %%%%%%%%%%%%%%%%%%%%%%%%%
\section{Motivation}
\label{sec:c1:mnpur}
This document represents the knowledge acquired both inside and outside the degree of Computer Engineering at the University of Alicante.

This thesis arose due to a collaboration with the \ac{dtic} department, specifically with the \textit{3D Perception Lab} group. The main research area of this group focuses on the intersection of Machine Learning (Deep Learning specifically), 3D Computer Vision, and \ac{gpu} Computing. 

This collaboration addresses several projects, one of which was presented at the IROS technology conference in 2018, \textit{The RobotriX: An eXtremely Photorealistic and Very-Large-Scale Indoor Dataset of Sequences with Robot Trajectories and Interactions} \cite{Garcia_Garcia_2018}. RobotriX is funded by \textit{Ministerio de Economia y Competitividad} of the Spanish Government as part of the project \ac{combaho}: system for enhancing autonomy of people with acquired brain injury and dependent on their integration into society (TIN2016-76515-R), and
counts with the participation of Jose Garcia-Rodriguez and Miguel Angel Cazorla-
Quevedo as main researchers, both being professors at the University of Alicante. UnrealROX is the main tool used to generate the data of such dataset, which will be detailed later in this document.

Personally, I believe that generating synthetic data to train new systems is a necessity, since there is a current human dependency to collect data for datasets. Game engines with advanced graphics capabilities can accelerate this task by working with synthetic data. In order to achieve this, there is the need to create a framework capable of recording and generating synthetic data, that is why UnrealROX was born.

UnrealROX was already created when I entered the RobotriX project. It was my task to coordinate the main refactor of the source code in order to improve its flexibility, effiency, and to bring it into a production state; progressively porting it to C++ and providing advice to the rest of the team members based on my previous experience with \ac{ue4}.

The main purpose of this project is to create a simulator that will serve to reduce the gap between synthetic and real data. Future works would have to demonstrate that the simulator is capable of generating samples that are easily transferred to a real-world domain. My main objective on this project is to ensure an efficient framework able to record at a minimum framerate of 60+ \ac{fps} and generate data as efficiently as possible since we are going to deal with huge amounts of data.

One proposal to test the performance of UnrealROX simulator is binary categorization by using UnrealGrasp \cite{oprea2019visually}, which allows us to grasp dynamic objects from a scene, extending it to classify when a user is touching or not a particular object. Therefore, the problem would be divided into two major classes, interaction and non-interaction. Once the dataset is obtained, we can train an architecture able to classify within this synthetic data both situations. The next step would be testing this same architecture with real data to see how it performs.

\begin{figure}[h]
	\centering
	\begin{subfigure}[b]{0.475\textwidth}
		\centering
		\includegraphics[width=\textwidth]{Figures/hyperrealisticsynthdata}
		\caption[]%
		{{\small \textit{Unreal Paris 2018} by Dereau Benoit.}}    
	\end{subfigure}
	\hfill
	\begin{subfigure}[b]{0.475\textwidth}  
		\centering 
		\includegraphics[width=\textwidth]{Figures/bySOA_Academy}
		\caption[]%
		{{\small \textit{Archviz for UE4} by SOA Academy.}}    
	\end{subfigure}
	\vskip\baselineskip
	\begin{subfigure}[b]{0.475\textwidth}   
		\centering 
		\includegraphics[width=\textwidth]{Figures/ue4-summer-house-archviz-project}
		\caption[]%
		{{\small \textit{Summer House Archviz Project} by Ervin Jesse.}}    
	\end{subfigure}
	\quad
	\begin{subfigure}[b]{0.475\textwidth}   
		\centering 
		\includegraphics[width=\textwidth]{Figures/PostSovietBathroomByWhiteNoiseTeam}
		\caption[]%
		{{\small \textit{Post soviet bathroom} by White Noise Team.}}    
	\end{subfigure}
	\caption[ Synthetic scene on UE4 ]
	{\small Synthetic scenes on UE4} 
	\label{fig:hyperrealisticsynthdata}
\end{figure}

Thanks to this work, we will be able to prove that synthetic datasets, such as \textit{The RobotriX} \cite{Garcia_Garcia_2018}, are valid for real-world problems, which means that the gap between real data and synthetic data (as seen in Figure \ref{fig:hyperrealisticsynthdata}) will be even smaller.

\section{Objectives}
\label{sec:c1:objectives}

In this section we are going to define the main objectives of this project. To do this, we must first describe the state of the system prior to this work in Section \ref{sec:c1:objectives_init}. Then, Section \ref{sec:c1:objectives_final} will describe the main objectives once seen the initial architecture and the process we followed to achieve the completion of the extensible framework.

\subsection{Initial architecture}
\label{sec:c1:objectives_init}
UnrealROX was designed to compensate the main absences of the current state of the art in terms of simulators. The first version of the environment consisted on a \ac{ue4} project with a premature grasping system (UnrealGrasp \cite{oprea2019visually}) that allowed an operator to interact with different objects in the scene by controlling an interchangeable 3D agent using the Oculus Rift headset and controllers. The project also allowed to record the state of the scene and the movements of the operator with a primitive version of our Tracker Actor, documented in Section \ref{sec:tracker}. 

\begin{figure}[h]
	\centering
	\includegraphics[width=0.8\textwidth]{Figures/primitive_rob}
	\caption{Some samples of the RobotriX dataset.}
	\label{fig:primitive_rob}
\end{figure}

UnrealROX had a single camera embedded in the head of the controlled agent, this camera would be used to generate data for our dataset. In order to create said data, the simulator would reproduce the recorded movements offline to generate ground truth and raw data making use of UnrealCV \cite{qiu2016unrealcv} to capture the scene from a specific camera. The weight of the algorithm fell on a prototype made in Python that made use of UnrealCV with an HTTP server, as seen in Figure 	\ref{fig:ucvarch}, and was therefore slow and limited to the features of the plugin. This first version of the project was the one used to generate The RobotriX\footnote{\url{https://github.com/3dperceptionlab/therobotrix}} \cite{Garcia_Garcia_2018} dataset (see Figure \ref{fig:primitive_rob}), which motivated the continuity of the simulator.

\begin{figure}[h]
	\centering
	\includegraphics[width=0.8\textwidth]{Figures/ucvarch}
	\caption[Architecture of UnrealCV. Figure from the documentation of UnrealCV]{Architecture of UnrealCV. Figure from the documentation of UnrealCV\footnotemark.}
	\label{fig:ucvarch}
\end{figure}
\footnotetext{\url{http://docs.unrealcv.org/en/latest/reference/architecture.html}}

\subsection{Final version}
\label{sec:c1:objectives_final}
Once the architecture of UnrealROX had been studied, a series of objectives were set to make the environment more flexible and efficient. One of the first priorities was to add support for on boarding cameras, since most robots in the public market integrate multiple cameras in different parts of their bodies (see Section \ref{sec:mcs}). In addition, we implemented external cameras to provide the robot with information that it is not able to perceive directly. 

\begin{figure}[h]
	\centering
	\includegraphics[width=1\textwidth]{Figures/multicammm}
	\caption{Multi-camera setup on UnrealROX.}
	\label{fig:multicammm}
\end{figure}

Adding more cameras increased considerably the computation time when we executed the algorithm to generate data, which meant that we needed to do something regarding the generation algorithm. The final decision was to get rid of the client Python application to communicate with the UnrealCV server, executing everything locally. This decreased considerably the execution time of the algorithm. Then, in order to not bound ourselves to UnrealCV, we decided to create a ground truth generator specific for UnrealROX, which allowed us to have more control over the generated data. We also added a way to select which type of data is relevant for the user, so we only generate the data that the user specifies on the generation settings. 

Furthermore, we were able to integrate support for the HTC Vive headset, which was possible thanks to the cooperation of the \ac{dtic} department. Adding support for other devices on \ac{ue4} is easy thanks to the input system and the flexible API that the engine provides.

Another of the great challenges that UnrealROX posed was to completely refactor the animation system in order to simplify the import of new 3D agents (skeletal meshes), for that, we implemented a custom animation node for the animation system of \ac{ue4} that enabled us to bulk a series of bones by name accompanied by the modified transform; this node is the responsible of setting the translation and rotation of all the bones that we record with the Tracker Actor. Another of my responsibilities in the project was to convert all the code to C++ to decrease compute time.

%todo Falta una sección de objetivos que deje bien claro lo que estamos persiguiendo con items. De dónde partíamos (un prototipo hecho en Python que hacía uso de UnrealCV con un servidor HTTP y que por lo tanto era lento y limitado) y a dónde queríamos llegar (captura de datos en realidad virtual, gestión de grasping de objetos, generador de modalidades de datos, grabaciones de secuencias a cierta velocidad...). Una lista más definida de lo que se trata de conseguir.


%\section{Planning}
%\label{sec:c1:planning}
% - Quizás una sección con un project con un poco de planificación del proyecto (tareas, estimaciones temporales, dependencias).